# Reference Pipeline for Java Application

This project is a reference of a end to end CI pipeline for a sample 3 tier application designed by HQ SA and hosted in AWS EC2:
* Web: HTML/AngularJS
* App: Java Spring boot
* Database: MySQL

<span style="color:red">Note: The source code for this application is not meant as a reference for writing good/secure code and is purely used to demonstrate the pipeline.</span>

## Table of contents:

- [Compliance Framework](#compliance-framework)
- [Pipeline](#pipeline)

## Compliance Framework

This project has [compliance framework: ship-hats-webapp-compliance](https://sgts.gitlab-dedicated.com/wog/ship-hats-compliance/-/tree/main#webapp-compliance) enabled by having compliance template included directly in its CI/CD configuration.

For more details, please refer to [Common Pre-requisites to Using Reference Pipelines - Compliance Framework](https://sgts.gitlab-dedicated.com/groups/wog/gvt/ctmo/reference-pipelines/-/wikis/Common-Pre-requisites-to-Using-Reference-Pipelines-(WIP)#compliance-framework).

## Pipeline
Pipeline is defined in [.gitlab-ci.yml](.gitlab-ci.yml) and it leverages templates in the inner-sourced project [SHIP-HATS Templates](https://gts.gitlab-dedicated.systems/templates/ship-hats-templates).

For pre-requisites, please refer to [Common Pre-requisites to Using Reference Pipelines](https://gts.gitlab-dedicated.systems/groups/ctmo/-/wikis/Common-Pre-requisites-to-Using-Reference-Pipelines-(WIP)).

For CD approaches, please refer to [Gitlab CD Approaches](https://sgts.gitlab-dedicated.com/groups/wog/gvt/ctmo/reference-pipelines/-/wikis/Gitlab-CD-Approaches).

### Setting Up and Tearing Down Automated Test Env
The automated test env is ephemeral and is meant to be used to test deployment and run automated testing e.g. API/UI testing and DAST. As there is no dependency between each tier, each tier is set up in parallel in a separate job. Each job is executed on a [remote Gitlab runner](#setting-up-remote-gitlab-runner). To ensure that this runner is used, a tag that is specific to this runner is added as a required tag for each job. 

Separate ansible playbooks are used to:
1. [Create baseline images](deploy/ansible-devenv/site.yml) required for the 3 tiers (Web, App, Database)
2. [Launch ec2 instances](deploy/ansible-devenv/setup-test-machines.yml) based on the variables ec2_class and env provided. 
  * The value of ec2_class is dependent on the tier:
    * Web: webservers
    * App: appservers
    * Database: databases
  * The value of env is dependent on the target enviornment:
    * Automated test environment: test  
    * SIT: sit
  * The configurations used to launch each ec2 instances are stored as variables in the ansible playbook. Each ec2 instance launched will have the following tags
    * purpose:test
    * class:{env}_{ec2_class} e.g. class:test_webservers 
3. [Terminate ec2 instances](deploy/ansible-devenv/terminate-test-machines.yml) based on the variables ec2_class and env provided.
  * To ensure that this ansible playbook has access to the AWS inventory, provide -i option and specify path of aws_ec2.yaml
  * To ensure that Ansible is able to establish a SSH connection to the target ec2 instance, provide path of SSH private key via the --private_key option as well as the SSH user via the --user option. The value of the private key can be stored as a project variable and output to a file before this task - please make sure to clean up this private key as part of a final task. 

Potential improvements:
* Configure custom domain name at least for web tier 
* Harden baseline images
* Set up test environment that resembles staging/production environment as much as possible 

References:
* [How to Setup Ansible AWS Dynamic Inventory](https://devopscube.com/setup-ansible-aws-dynamic-inventory/)
* [amazon.aws.aws_ec2 inventory – EC2 inventory source](https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html)
* [Ansible - Using Variables](http://docs.ansible.com/ansible/latest/playbooks_variables.html)
* [Ansible Role: MySQL](https://github.com/geerlingguy/ansible-role-mysql)

### Deploy to Automated Test Env
This involves populating the database and deploying the artifacts to application and web servers. The job is executed on [remote Gitlab runner](#setting-up-remote-gitlab-runner). To ensure that this runner is used, a tag that is specific to this runner is added as a required tag for each job. 

An [ansible playbook](deploy/ansible-devenv/deploy-app.yml) is used to perform the deployment as follows:
1. Copy sql script to the new database server
2. Create and populate tables in new database
3. Copy build artifacts for app tier to new app server
4. Copy build artifacts for web tier to new web server
5. Restart the app and web servers

Another [ansible playbook](deploy/ansible-devenv/save-ip-addresses.yml) is used to save the address of the web server to a shell script. This shell script is sourced before automated testing so that the automated testing knows how to reach the web server to perform the various tests. For both ansible playbooks, it is required to provide -i option and to specify path of aws_ec2.yaml so that AWS inventory is available to each playbook. For the deployment ansible playbook, it is also required to provide path of SSH private key via the --private_key option as well as the SSH user via the --user option.
 
Note:
* DB username, password are currently hardcoded
* App server is running as root
* IP addresses of DB and app server are fetched using 'groups' e.g. groups['tag_class_test_databases'][0] in playbook
* To ensure that web and app server configuration files are updated with the correct IP address:
  1. use set_fact task to set a host fact to the retrieved IP address in playbook
  2. reference the fact as a variable using Jinja2 templating system in the web and app server configuration files e.g.  {{ db_ip_address }} 
  3. use template task to copy the web and app server configuration files to the target systems in playbook

Potential improvements:
* Store DB username, password as project variables (and eventually in a key vault) 
* Use accounts with least privileges required to run app/web servers
* Use a key vault to store and check out SSH keys for the ec2 instances in the target environment

## Setting Up Remote Gitlab Runner

Set up AWS EC2 instance to host remote Gitlab runner, which is used to setup/teardown the test environment as well as to perform deployment. 

For the instructions in this documentation, we are using:
- Platform: RHEL (Red Hat Enterprise Linux)
- AMI: RHEL-8.5.0_HVM-20211103-x86_64-0-Hourly2-GP2

You may use a newer AMI if available.


SSH into the EC2 instance.
The default user will be ec2-user.


Download installation binary:

`sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"`

Give permission to execute:

`sudo chmod +x /usr/bin/gitlab-runner`

Create a Gitlab CI user:

`sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash`


Install and run as service:

`sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner` 
`sudo gitlab-runner start`


References:

User guide for runner install:
https://docs.gitlab.com/runner/install/
Check first section to make sure your computing architecture is supported.

The documentation for installation of Gitlab runner in Linux.
https://docs.gitlab.com/runner/install/linux-manually.html



### Register Gitlab runner

This section is for registration of Gitlab runner with the Gitlab server.

There are 3 types of runner:
- Shared Runner
- Group Runner
- Project-specific Runner

The following steps will be for registering a project-specific runner. 
This is suitable if you are intending to provision a runner for your project.
Note that a single runner can be registered with one or multiple projects.

Get the Gitlab server URL and token for connecting your Gitlab runner from the Gitlab project.
In Gitlab, go to project Setting > CI/CD and expand the Runner section.
Note the server URL and token.

On the EC2 instance that has the Gitlab runner installed, perform the registration. Run the following command:

`sudo gitlab-runner register`

Use the Gitlab URL and token from above for the registration.
After registration is completed, you will see the newly registered runner in "Specific runners" section.


References:

https://docs.gitlab.com/runner/register/index.html



### Install zip / unzip

As ec2-user, run the following command to install zip and unzip. 

`sudo yum install zip`
`sudo yum install unzip`


### Install Ansible 

Check if Python3 is installed.
You may run the following command:

`python --version`


For the Enterprise Redhat Linux EC2 AMI, the location of Python is at:

/usr/bin


If not installed, install Python3 using the following command:

`sudo yum install python39`

Create a soft link:

`sudo ln -s /usr/bin/python3.9 /usr/bin/python`

Test run:

`python --version`

Make sure pip3 is installed. Run:

`pip3 --version`

Change user to gitlab-runner:

`sudo su - gitlab-runner`

Install Ansible under user gitlab-runner:

`pip3 install ansible`


Validate the installation of Ansible:

`ansible --version`




Install boto3:

`pip3 install boto3`



### Install AWS CLI

Download the latest AWS CLI:

`curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"`

Unzip the AWS CLI bundle:

`unzip awscli-bundle.zip`

Install AWS CLI:

`sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws`

Validate the installation of AWS CLI:

`aws --version`

Sample response:
`aws-cli/1.24.1 Python/3.9.7 Linux/4.18.0-348.el8.x86_64 botocore/1.26.1`

References:

https://docs.aws.amazon.com/cli/v1/userguide/install-linux.html


### Configuring AWS Profile
1. Create a custom AWS IAM policy to allow
  * creation and tagging of ec2 instances
    * restricted to instances in specific subnets
  * starting, stopping and termination of instances
    * restricted to instances with tag 'purpose:test'
  * Describe* actions for ec2
    * required for retrieving the inventory
2. Create a AWS IAM role and assign custom policy to this role
3. Assign new role to Ansible ec2 instance
  * This allows Ansible to invoke AWS APIs without requiring access key and secret key to be provided.

### Retrieving the Inventory

In the directory where you will run the Ansible playbook, create the 
* [ansible.cfg](deploy/ansible-devenv/ansible.cfg) file
  Put the following content in the file:

  ```
  [inventory]
  enable_plugins=aws_ec2

  [defaults]
  host_key_checking=False
  ```
  In particular, to disable host check to avoid prompting for host checks for newly launched ec2 instances

* Create a file [aws_ec2.yaml](deploy/ansible-devenv/aws_ec2.yaml) e.g. with the following content

  ```
  plugin: aws_ec2
  regions:
    - ap-southeast-1
  keyed_groups:
    - key: tags
      prefix: tag
      trailing_separator: False
    - key: placement.region
      prefix: aws_region
  filters:
    # All instances with their `purpose` tag set to `test`
    tag:purpose: test
    instance-state-name: running
  cache_timeout: 0
  ```
  In particular, to disable caching as caching can cause a problem if you are launching and terminating ec2 instances very frequently.


Run the following command to retrieve the list of hosts:

`ansible-inventory -i ./aws_ec2.yaml --list -vvv`


References:
* [How to Setup Ansible AWS Dynamic Inventory](https://devopscube.com/setup-ansible-aws-dynamic-inventory/)
* [amazon.aws.aws_ec2 inventory – EC2 inventory source](https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html)



### Running the Ansible playbook

Use the following command to run your Ansible playbook e.g.

`ansible-playbook -i ./aws_ec2.yaml ./<playbook>.yml --extra-vars "<vars>" --private-key=<private key>.pem --user=ec2-user -vvv`

In particular, 
- -i option to specify path of aws_ec2.yaml so that AWS inventory is available to playbook (if required)
-  specify private key (with --private-key option) and/or user (with --user option) for ansible to be able to successfully establish SSH connection to the target machine






