#!groovy

{  String appGit, String cicdGit, boolean doProvision ->

	echo "doProvision=${doProvision}"

	node("master") {
		
		stage("Setup") {
			dir('clglabapp') {
				checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[url: appGit]]]
			}

			dir('clglabapp-cicd') {
				checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[url: cicdGit]]]
			}
		}
		
		stage("Select and Download Artifacts from Nexus OSS") {
			sh "rm -rf artifacts"
			sh "wget -O apps.json 'http://10.0.1.53:8081/service/rest/beta/search?repository=maven-releases&name=testlab-app'"
			apps = readJSON file: 'apps.json', text: ''
			
			def list = []
			for (item in apps.items) { 
				list << item.version
			}
		
			selectedVersion = input( id: 'userInput', message: 'Choose version of artifact to deploy to SIT', parameters: [ [$class: 'ChoiceParameterDefinition', choices: list.join("\n"), description: 'versions', name: 'version'] ], submitter: 'linda_chang,debbie_jy_ang')
			
			dir('artifacts') {
				//download app artifact
				sh "wget -O testlab_app.jar 'http://10.0.1.53:8081/service/rest/beta/search/assets/download?repository=maven-releases&name=testlab-app&maven.extension=jar&version=${selectedVersion}'"
				
				//download web artifacts
				sh "wget -O testlab_web.zip 'http://10.0.1.53:8081/service/rest/beta/search/assets/download?repository=maven-releases&name=testlab-web&maven.extension=zip&version=${selectedVersion}'"
				sh 'unzip testlab_web.zip'
			}
		}
		
		stage('Deploy to SIT Env') {
			sh 'cp -a artifacts/public clglabapp-cicd/deploy/ansible-devenv/files/'
			sh 'cp -a artifacts/testlab_app.jar clglabapp-cicd/deploy/ansible-devenv/files/'
			sh 'cp -a clglabapp/testlab-app/dbscripts/MASTER_CREATE_TABLE_SCRIPT.sql clglabapp-cicd/deploy/ansible-devenv/files/'
			sh 'zip -r ansible-devenv.zip clglabapp-cicd/deploy/ansible-devenv'
			stash includes: "ansible-devenv.zip", name: 'ansible-scripts' 
			
			node('ansible-controller') {
				sh 'rm -rf clglabapp-cicd'
				sh 'rm -rf ansible-devenv.zip'
				unstash 'ansible-scripts'
				sh 'unzip ansible-devenv.zip'
				
				dir('clglabapp-cicd/deploy/ansible-devenv') {
				
					if (doProvision) {						
						sh '/usr/local/bin/ansible-playbook -i /etc/ansible/ec2.py terminate-test-machines.yml  --extra-vars "ec2_class=databases env=sit"'
						sh '/usr/local/bin/ansible-playbook -i /etc/ansible/ec2.py terminate-test-machines.yml  --extra-vars "ec2_class=webservers env=sit"'
						sh '/usr/local/bin/ansible-playbook -i /etc/ansible/ec2.py terminate-test-machines.yml  --extra-vars "ec2_class=appservers env=sit"'
						sh '/usr/local/bin/ansible-playbook setup-test-machines.yml --extra-vars "ec2_class=databases env=sit"'
						sh '/usr/local/bin/ansible-playbook setup-test-machines.yml --extra-vars "ec2_class=webservers env=sit"'
						sh '/usr/local/bin/ansible-playbook setup-test-machines.yml --extra-vars "ec2_class=appservers env=sit"'
						sh '/usr/local/bin/ansible-playbook -i /etc/ansible/ec2.py deploy-app.yml --extra-vars "env=sit"'
					} else {
						sh '/usr/local/bin/ansible-playbook -i /etc/ansible/ec2.py deploy-artifacts.yml --extra-vars "env=sit"'
					}
				}
			}
		}
	}
}