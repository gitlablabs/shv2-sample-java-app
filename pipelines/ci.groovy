#!groovy

{ String appGit, String cicdGit ->
	node("master") {
		
		def version
		def artifactId
					
		stage("Setup") {
			dir('clglabapp') {
				checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[url: appGit]]]
			}

			dir('clglabapp-cicd') {
				checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[url: cicdGit]]]
			}
		}
		
		stage("Build App") {
			dir("clglabapp/testlab-app") {
				withMaven(maven: 'Maven 3.5.2') {
					def pom = readMavenPom file: 'pom.xml'
					version = pom.version.replace("-SNAPSHOT", ".${currentBuild.number}")
					artifactId = pom.artifactId
					
					sh "mvn versions:set -DnewVersion=${version} -B"
					sh "mvn clean package verify"
				}

			}
		}
		
		stage("Build Web") {
			dir("clglabapp/testlab-web") {
				env.NODEJS_HOME = "${tool 'NodeJS 9.8.0'}"
				// on linux / mac
				env.PATH="${env.NODEJS_HOME}/bin:${env.PATH}"
				sh "npm --no-git-tag-version version ${version}" 
				sh 'npm run-script build-public'
				
				dir("build/dist/sit") {
					sh 'zip -r public.zip public'
				}
			}
		}
		
		stage("Sonarqube Scan App") {
			dir("clglabapp/testlab-app") {
				withMaven(maven: 'Maven 3.5.2') {
					withSonarQubeEnv {
						sh 'mvn sonar:sonar -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.login=$SONAR_AUTH_TOKEN'
					}
				}

			}
			
			timeout(time: 1, unit: 'HOURS') {
				def qg = waitForQualityGate()
				if (qg.status != 'OK') {
					echo "Quality Gate Status: ${qg.status} - set currentBuild to UNSTABLE"
					//error "Failed Sonarqube Quality Gate"
					currentBuild.result = 'UNSTABLE'
				}
			}
		}
		
		stage('OWASP Dependency Check Scan') {
		    dependencyCheckAnalyzer datadir: '', hintsFile: '', includeCsvReports: false, includeHtmlReports: true, includeJsonReports: false, includeVulnReports: true, isAutoupdateDisabled: false, outdir: '', scanpath: "${WORKSPACE}/clglabapp", skipOnScmChange: false, skipOnUpstreamChange: false, suppressionFile: "${WORKSPACE}/clglabapp-cicd/appsec/DependencyCheckSuppressionFile.xml", zipExtensions: ''
		
		    dependencyCheckPublisher defaultEncoding: '', healthy: '', pattern: '', shouldDetectModules: true, unHealthy: '', unstableTotalHigh: '0', unstableTotalNormal: '0'
		    
		}
		
		stage('Deploy to Dev Env') {
			sh 'cp -a clglabapp/testlab-web/build/dist/sit/public clglabapp-cicd/deploy/ansible-devenv/files/'
			sh 'cp -a clglabapp/testlab-app/dbscripts/MASTER_CREATE_TABLE_SCRIPT.sql clglabapp-cicd/deploy/ansible-devenv/files/'
			sh 'cp -a clglabapp/testlab-app/target/*.jar clglabapp-cicd/deploy/ansible-devenv/files/testlab_app.jar'
			sh 'zip -r ansible-devenv.zip clglabapp-cicd/deploy/ansible-devenv'
			stash includes: "ansible-devenv.zip", name: 'ansible-scripts' 
			
			node('ansible-controller') {
				sh 'rm -rf clglabapp-cicd'
				sh 'rm -rf ansible-devenv.zip'
				unstash 'ansible-scripts'
				sh 'unzip ansible-devenv.zip'
				
				dir('clglabapp-cicd/deploy/ansible-devenv') {
					sh '/usr/local/bin/ansible-playbook -i /etc/ansible/ec2.py terminate-test-machines.yml  --extra-vars "ec2_class=databases env=test"'
					sh '/usr/local/bin/ansible-playbook -i /etc/ansible/ec2.py terminate-test-machines.yml  --extra-vars "ec2_class=webservers env=test"'
					sh '/usr/local/bin/ansible-playbook -i /etc/ansible/ec2.py terminate-test-machines.yml  --extra-vars "ec2_class=appservers env=test"'
					sh '/usr/local/bin/ansible-playbook setup-test-machines.yml --extra-vars "ec2_class=databases env=test"'
					sh '/usr/local/bin/ansible-playbook setup-test-machines.yml --extra-vars "ec2_class=webservers env=test"'
					sh '/usr/local/bin/ansible-playbook setup-test-machines.yml --extra-vars "ec2_class=appservers env=test"'
					sh '/usr/local/bin/ansible-playbook -i /etc/ansible/ec2.py deploy-app.yml --extra-vars "env=test"'
					sh '/usr/local/bin/ansible-playbook -i /etc/ansible/ec2.py save-ip-addresses.yml --extra-vars "env=test"'
					
					stash includes: "ipaddresses.properties", name: 'ipaddresses'
				}
			}
		}
		
		stage('Run Api Tests') {

			sh 'zip -r apitest.zip clglabapp/test-automation/APITest'
			stash includes: "apitest.zip", name: 'ansibleapi-scripts' 

			node('selenium-server') {

				sh 'rm -rf clglabapp'
				sh 'rm -rf apitest.zip'
				unstash 'ansibleapi-scripts'
				sh 'unzip apitest.zip'


				 env.NODEJS_HOME = "${tool 'NodeJS 9.8.0'}"
				 env.PATH="${env.NODEJS_HOME}/bin:${env.PATH}"
				 
				 dir('clglabapp/test-automation/APITest') {
				 
					unstash 'ipaddresses'
                    sh "python UpdateApiTestIpAddress.py"
                    sh "newman run GovTech_CLG.postman_collection.json --reporters html --reporter-html-export /home/ec2-user/jenkins-agent/postman-scripts/apitestreport.html"
                 } 
				 
                 dir('/home/ec2-user/jenkins-agent') {
    				publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'postman-scripts', reportFiles: 'apitestreport.html', reportName: 'Api Test HTML Report', reportTitles: ''])
    			 } 
			}
		}
		
		stage('Run Selenium Tests') {
			
			sh 'zip -r funtionaltest.zip clglabapp/test-automation/FunctionalTest'
			stash includes: "funtionaltest.zip", name: 'ansiblefunction-scripts' 
			
			node('selenium-server') {

				sh 'rm -rf clglabapp'
				sh 'rm -rf funtionaltest.zip'
				unstash 'ansiblefunction-scripts'
				sh 'unzip funtionaltest.zip'

				dir('clglabapp/test-automation/FunctionalTest') {
					unstash 'ipaddresses'
					
					withMaven(maven: 'Maven 3.5.2') {
						exitCode = sh returnStatus: true, script: "mvn clean test"
						echo "exitCode=${exitCode}"
						
						if (exitCode != 0){
							echo 'Failed test cases - set current build to UNSTABLE'
							//error "Failed OWAS ZAP Passive Scan"
							currentBuild.result = 'UNSTABLE'
						}
					}
				}
			}
		}
		
		stage('OWASP ZAP Passive Scan') {
			unstash 'ipaddresses'
			def ipaddresses = readProperties file: 'ipaddresses.properties'
			sh 'cat ipaddresses.properties'
			
			node('zap-docker') {
				exitCode = sh returnStatus: true, script: "docker run --rm -v /home/ec2-user/jenkins_agent/zap-data:/zap/wrk/:rw -t owasp/zap2docker-stable zap-baseline.py -t http://${ipaddresses['webserver_ip_address']} -r testreport.html -c testconfig.conf"
				
				echo "exitCode=${exitCode}"
				
				dir('/home/ec2-user/jenkins_agent') {
					publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'zap-data', reportFiles: 'testreport.html', reportName: 'OWASP ZAP HTML Report', reportTitles: ''])
				} 
				
				if (exitCode == 1) {
					echo 'Fail count > 0 - set current build to UNSTABLE'
					//error "Failed OWAS ZAP Passive Scan"
					currentBuild.result = 'UNSTABLE'
				}
			}
		}
		
		stage('Release') {
			dir("clglabapp") {
				dir("testlab-app") { 
					withMaven(maven: 'Maven 3.5.2') {
						sh "mvn deploy -Dmaven.install.skip=true -Dmaven.test.skip=true"
						sh "mvn deploy:deploy-file -DgroupId=sg.tech.testlab -DartifactId=testlab-web -Dversion=${version} -DgeneratePom=true -Dpackaging=zip -DrepositoryId=nexus-releases -Durl=http://10.0.1.53:8081/repository/maven-releases/ -Dfile=../testlab-web/build/dist/sit/public.zip"
						sh "git tag -a ${artifactId}-${version} -m 'tag ${artifactId}-${version}'"
						sh "git push --tags"
					}
				}
			}
			
			dir("clglabapp-cicd") {
				sh "git tag -a ${artifactId}-${version} -m 'tag ${artifactId}-${version}'"
				sh "git push --tags"
			}
		}
	}
}