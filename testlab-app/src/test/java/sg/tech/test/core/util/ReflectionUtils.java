package sg.tech.test.core.util;

public class ReflectionUtils {
	public static <T> T setValue(T targetObject, String fieldName, Object valueObject) {
		org.springframework.util.ReflectionUtils.setField(
				org.springframework.util.ReflectionUtils.findField(targetObject.getClass(), fieldName),
				targetObject, valueObject);
		return targetObject;
	}
}
