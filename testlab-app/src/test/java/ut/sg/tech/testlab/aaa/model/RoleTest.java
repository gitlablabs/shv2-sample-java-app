package ut.sg.tech.testlab.aaa.model;

import sg.tech.test.core.util.AbstractModelTest;
import sg.tech.testlab.aaa.model.Role;

public class RoleTest extends AbstractModelTest<Role> {

	@Override
	protected Role getInstance() {
		return new Role();
	}

}
