package ut.sg.tech.testlab.aaa.model;

import sg.tech.test.core.util.AbstractModelTest;
import sg.tech.testlab.aaa.model.User;

public class UserTest extends AbstractModelTest<User> {

	@Override
	protected User getInstance() {
		return new User();
	}
}
