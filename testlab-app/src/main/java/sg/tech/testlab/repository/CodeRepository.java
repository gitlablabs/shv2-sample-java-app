package sg.tech.testlab.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sg.tech.testlab.model.Code;

@Repository
public interface CodeRepository extends JpaRepository<Code, Long> {
	public List<Code> findByCategory(String category);

	@Query("SELECT c FROM Code c WHERE c.category = :category ORDER BY c.displayOrder ASC, c.value ASC")
	public List<Code> findOrderedListByCategory(@Param("category") String category);
}
