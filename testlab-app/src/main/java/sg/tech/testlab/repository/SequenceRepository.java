package sg.tech.testlab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sg.tech.testlab.model.Sequence;

@Repository
public interface SequenceRepository extends JpaRepository<Sequence, Long> {
	public Sequence getByKey(String key);
}
