package sg.tech.testlab.repository;

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import sg.tech.testlab.aaa.model.User;

public interface SearchRepository {
	public Page<Long> searchByUser(PageRequest pageRequest, User user, String applicationRefNo, Long applicationTypecd, String applicantName,
			String applicantNric, Long applicationStatusCd, Map<Object, Object> sortMap) throws Exception;
}
