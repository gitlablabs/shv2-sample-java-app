package sg.tech.testlab.repository.nativeimpl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import sg.tech.testlab.aaa.model.User;
import sg.tech.testlab.repository.SearchRepository;

@Configuration
public class SearchRepositoryMySqlImpl implements SearchRepository {
	private static final Logger logger = LoggerFactory.getLogger(SearchRepositoryMySqlImpl.class);

	private DataSource dataSource;

	public SearchRepositoryMySqlImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Page<Long> searchByUser(PageRequest pageRequest, User user, String applicationRefNo, Long applicationTypeCd, String applicantName,
			String applicantNric, Long applicationStatusCd, Map<Object, Object> sortMap) throws Exception {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();

			String sql = "FROM APPLN_APPLICATION appln_"
				+ " INNER JOIN APPLN_APPLN_TYPE applnType_ ON applnType_.APPLNTYPEID = appln_.APPLNTYPEID"
				+ " INNER JOIN AAA_USER creator_ ON creator_.USERID = appln_.CREATEBY"
				 + " LEFT JOIN CORE_CODE statusCd_ ON statusCd_.CODEID = appln_.STATUSCD"
			         + " WHERE 1 = 1";

			if (applicationRefNo != null && !applicationRefNo.isEmpty()) {
				sql += " AND appln_.APPLN_REF_NO LIKE '" + applicationRefNo + "'";
			}

			if (applicationTypeCd != null) {
				sql += " AND appln_.APPLNTYPEID = " + applicationTypeCd;
			}

			if (applicantName != null && !applicantName.isEmpty()) {
				sql += " AND appln_.APPLT_NAME LIKE '" + applicantName + "'";
			}

			if (applicantNric != null && !applicantNric.isEmpty()) {
				sql += " AND appln_.APPLT_NRIC LIKE '" + applicantNric + "'";
			}

			if (applicationStatusCd != null) {
				sql += " AND appln_.STATUSCD = " + applicationStatusCd;
			}

			sql += " AND CREATEBY = " + user.getId();

			int count = -1;

			String countSql = "SELECT COUNT(1) AS TOTAL " + sql;
			logger.debug(countSql);
			Statement countStatement = connection.createStatement();
			ResultSet rs = countStatement.executeQuery(countSql);
			while (rs.next()) {
				count = rs.getInt("TOTAL");
			}

			logger.debug("Total: " + count);

			List<Long> applicationIdList = new ArrayList<Long>();
			String selectSql = "SELECT appln_.APPLICATIONID AS APPLICATIONID " + sql;
			
			logger.debug(sortMap.toString());
			if (sortMap != null && !sortMap.isEmpty()) {
				String orderSql = "";
				for (Object keyObj : sortMap.keySet()) {
					String key = (String) keyObj;

					String orderByKey = "";
					if ("applicationRefNo".equals(key)) {
						orderByKey = "appln_.APPLN_REF_NO";
					} else if ("applicationType".equals(key)) {
						orderByKey = "applnType_.DESC_";
					} else if ("applicantName".equals(key)) {
						orderByKey = "appln_.APPLT_NAME";
					} else if ("applicantNric".equals(key)) {
						orderByKey = "appln_.APPLT_NRIC";
					} else if ("createdBy".equals(key)) {
						orderByKey = "creator_.NAME";
					} else if ("createdOn".equals(key)) {
						orderByKey = "appln_.CREATEON";
					} else if ("applicationStatus".equals(key)) {
						orderByKey = "statusCd_.VALUE_";
					}
					
					orderSql += (orderSql.isEmpty() ? " ORDER BY " : ", ") + orderByKey + " " + sortMap.get(key);
				}
				selectSql += orderSql;
			}
			if (pageRequest != null) {
				selectSql += " LIMIT " + (pageRequest.getPageNumber() * pageRequest.getPageSize()) + ","
						+ pageRequest.getPageSize();
			}
			logger.debug("Select SQL: " + selectSql);

			Statement selectStatement = connection.createStatement();
			ResultSet selectRs = selectStatement.executeQuery(selectSql);
			while (selectRs.next()) {
				Long applicationId = selectRs.getLong("APPLICATIONID");
				applicationIdList.add(applicationId);
			}

			return new PageImpl<Long>(applicationIdList, pageRequest, count);
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
