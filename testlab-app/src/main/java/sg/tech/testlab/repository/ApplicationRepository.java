package sg.tech.testlab.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sg.tech.testlab.model.Application;
import sg.tech.testlab.model.Code;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {
//	@Query(
//		value = "SELECT a FROM Application a WHERE a.status = :status",
//		countQuery = "SELECT COUNT(*) FROM Application a WHERE a.status = :status"
//	)
	public Page<Application> findByStatus(Pageable pageRequest, @Param("status") Code status);
}
