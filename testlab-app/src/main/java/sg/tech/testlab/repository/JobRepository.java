package sg.tech.testlab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sg.tech.testlab.model.Job;

@Repository
public interface JobRepository extends JpaRepository<Job, Long> {
}
