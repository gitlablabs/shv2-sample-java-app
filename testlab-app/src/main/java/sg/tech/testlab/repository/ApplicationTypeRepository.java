package sg.tech.testlab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sg.tech.testlab.model.ApplicationType;

@Repository
public interface ApplicationTypeRepository extends JpaRepository<ApplicationType, Long> {
}
