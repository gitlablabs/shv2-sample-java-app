package sg.tech.testlab.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sg.tech.testlab.aaa.model.User;
import sg.tech.testlab.aaa.service.UserService;
import sg.tech.testlab.model.Application;
import sg.tech.testlab.model.ApplicationType;
import sg.tech.testlab.model.Code;
import sg.tech.testlab.repository.ApplicationRepository;
import sg.tech.testlab.repository.ApplicationTypeRepository;
import sg.tech.testlab.repository.SearchRepository;
import sg.tech.testlab.service.ApplicationService;
import sg.tech.testlab.service.CodeService;
import sg.tech.testlab.service.SequenceService;
import sg.tech.testlab.util.UserContext;

@Service
public class ApplicationServiceImpl implements ApplicationService {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(ApplicationServiceImpl.class);

	@Autowired
	public ApplicationTypeRepository applicationTypeRepository;

	@Autowired
	public ApplicationRepository applicationRepository;

	@Autowired
	public SequenceService sequenceService;

	@Autowired
	public CodeService codeService;
	
	@Autowired
	public UserService userService;
	
	@Autowired
	public SearchRepository searchRepository;
	
	@Override
	public ApplicationType getApplicationTypeById(Long id) {
		return this.applicationTypeRepository.findOne(id);
	}

	@Override
	public List<ApplicationType> findAllApplicationType() {
		return this.applicationTypeRepository.findAll();
	}

	@Override
	public Application createApplication(Long applicationTypeCd, String appltName, String appltNric, Date appltDob,
			Long appltGenderCd, Long appltBirthCountryCd, Long appltBirthStateCd, Long appltRaceCd, Long appltReligionCd, List<Long> languageCdList) {

//		logger.debug("applicationTypeCd [" + applicationTypeCd + "]");
//		logger.debug("appltName [" + appltName + "]");
//		logger.debug("appltNric [" + appltNric + "]");
//		logger.debug("appltDob [" + appltDob + "]");
//		logger.debug("appltGenderCd [" + appltGenderCd + "]");
//		logger.debug("appltBirthCountryCd [" + appltBirthCountryCd + "]");
//		logger.debug("appltRaceCd [" + appltRaceCd + "]");
//		logger.debug("appltReligionCd [" + appltReligionCd + "]");
//		logger.debug("languageCdList [" + languageCdList + "]");
		
		User user = null;
		String userName = UserContext.getUserName();
		if (userName == null || userName.isEmpty()) {
			throw new RuntimeException("Not Authorized");
		} else {
			user = this.userService.getUserByUserName(userName);
		}

		ApplicationType applicationType = getApplicationTypeById(applicationTypeCd);
		if (applicationType == null) {
			throw new IllegalArgumentException("Invalid Application Type");
		}

		Code appltGender = null;
		if (appltGenderCd != null) {
			appltGender = this.codeService.getCodeById(appltGenderCd);
			if (appltGender == null) {
				throw new IllegalArgumentException("Invalid Gender");
			}
		}

		Code appltBirthCountry = null;
		if (appltBirthCountryCd != null) {
			appltBirthCountry = this.codeService.getCodeById(appltBirthCountryCd);
			if (appltBirthCountry == null) {
				throw new IllegalArgumentException("Invalid Country of Birth");
			}
		}

		Code appltBirthState = null;
		if (appltBirthStateCd != null) {
			appltBirthState = this.codeService.getCodeById(appltBirthStateCd);
			if (appltBirthState == null) {
				throw new IllegalArgumentException("Invalid State of Birth");
			}
		}

		Code appltRace = null;
		if (appltRaceCd != null) {
			appltRace = this.codeService.getCodeById(appltRaceCd);
			if (appltRace == null) {
				throw new IllegalArgumentException("Invalid Race");
			}
		}

		Code appltReligion = null;
		if (appltReligionCd != null) {
			appltReligion = this.codeService.getCodeById(appltReligionCd);
			if (appltReligion == null) {
				throw new IllegalArgumentException("Invalid Religion");
			}
		}

		Application application = new Application();
		application.setApplicationType(applicationType);
		application.setApplicantName(appltName);
		application.setApplicantNric(appltNric);
		application.setApplicantDob(appltDob);
		application.setApplicantGender(appltGender);
		application.setApplicantBirthCountry(appltBirthCountry);
		application.setApplicantBirthState(appltBirthState);
		application.setApplicantRace(appltRace);
		application.setApplicantReligion(appltReligion);

		List<Code> languageList = new ArrayList<Code>();
		for (Long languageCd : languageCdList) {
			Code language = this.codeService.getCodeById(languageCd);
			languageList.add(language);
		}
		application.setApplicantLanguages(languageList);

		String applicationRefNo = this.sequenceService.getNextFormattedSequence("APPLN_REF_NO");
		application.setApplicationRefNo(applicationRefNo);
		application.setStatus(this.codeService.getCodeById(6001L));

		application.setCreatedBy(user);
		application.setCreatedOn(new Date());


		return this.applicationRepository.saveAndFlush(application);
	}

	@Override
	public Application getApplicationById(Long id) {
		return this.applicationRepository.findOne(id);
	}

	@Override
	public Page<Application> searchByUser(PageRequest pageRequest, User user, String applicationRefNo, Long applicationTypecd,
			String applicantName, String applicantNric, Long applicationStatusCd, Map<Object, Object> sortMap)
			throws Exception {

		Page<Long> longPage = this.searchRepository.searchByUser(pageRequest, user, applicationRefNo, applicationTypecd, applicantName, applicantNric, applicationStatusCd, sortMap);

		Map<Long, Application> applicationIdToObjMap = new HashMap<Long, Application>();
		if (longPage.getContent() != null && !longPage.getContent().isEmpty()) {
			this.applicationRepository.findAll(longPage.getContent()).parallelStream().forEach(application -> {
				applicationIdToObjMap.put(application.getId(), application);
			});
		}

		List<Application> applnList = new ArrayList<Application>();
		for (Long applicationId : longPage.getContent()) {
			applnList.add(applicationIdToObjMap.get(applicationId));
		}

		return new PageImpl<Application>(applnList, pageRequest, longPage.getTotalElements());
	}
	
	@Override
	public void withdrawApplication(Long applicationId) {
		Application application = this.getApplicationById(applicationId);
		application.setStatus(this.codeService.getCodeById(6004L));
		this.applicationRepository.save(application);
	}
	
	@Override
	public Page<Application> findByStatus(Pageable pageRequest, Code status) {
		return this.applicationRepository.findByStatus(pageRequest, status);
	}


	@Override
	public void approveApplication(Long applicationId) {
		Application application = this.getApplicationById(applicationId);
		application.setStatus(this.codeService.getCodeById(6002L));
		this.applicationRepository.save(application);
	}

	@Override
	public void rejectApplication(Long applicationId) {
		Application application = this.getApplicationById(applicationId);
		application.setStatus(this.codeService.getCodeById(6003L));
		this.applicationRepository.save(application);
	}
}
