package sg.tech.testlab.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sg.tech.testlab.model.Code;
import sg.tech.testlab.repository.CodeRepository;
import sg.tech.testlab.service.CodeService;

@Service
public class CodeServiceImpl implements CodeService {

	@Autowired
	public CodeRepository codeRepository;

	@Override
	public Code getCodeById(Long id) {
		return this.codeRepository.findOne(id);
	}

	@Override
	public List<Code> findByCategory(String category) {
		return this.codeRepository.findByCategory(category);
	}

	@Override
	public List<Code> findOrderedListByCategory(String category) {
		return this.codeRepository.findOrderedListByCategory(category);
	}
}
