package sg.tech.testlab.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import sg.tech.testlab.aaa.model.User;
import sg.tech.testlab.model.Application;
import sg.tech.testlab.model.ApplicationType;
import sg.tech.testlab.model.Code;

public interface ApplicationService {
	public ApplicationType getApplicationTypeById(Long id);

	public List<ApplicationType> findAllApplicationType();

	public Application createApplication(Long applicationTypeCd, String appltName, String appltNric, Date appltDob,
			Long appltGenderCd, Long appltBirthCountryCd, Long appltBirthStateCd, Long appltRaceCd,
			Long appltReligionCd, List<Long> languageCdList);

	public Application getApplicationById(Long id);

	public Page<Application> searchByUser(PageRequest pageRequest, User user, String applicationRefNo, Long applicationTypecd,
			String applicantName, String applicantNric, Long applicationStatusCd, Map<Object, Object> sortMap)
			throws Exception;

	public void withdrawApplication(Long applicationId);

	public Page<Application> findByStatus(Pageable pageRequest, Code status);

	public void approveApplication(Long applicationId);
	public void rejectApplication(Long applicationId);
}
