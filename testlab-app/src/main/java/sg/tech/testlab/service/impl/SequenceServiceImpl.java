package sg.tech.testlab.service.impl;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sg.tech.testlab.model.Sequence;
import sg.tech.testlab.repository.SequenceRepository;
import sg.tech.testlab.service.SequenceService;

@Service
public class SequenceServiceImpl implements SequenceService {

	@Autowired
	public SequenceRepository sequenceRepository;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getNextFormattedSequence(String key) {
		Sequence sequence = sequenceRepository.getByKey(key);
		Long nextSeq = sequence.getLastSeq() + 1;
		if (nextSeq > sequence.getMax()) {
			nextSeq = 1L;
		}
		sequence.setLastSeq(nextSeq);

		StringBuffer formattedSeq = new StringBuffer();
		String formatPattern = sequence.getPattern();
		
		Matcher matcher = Pattern.compile("\\[(.*?)\\]").matcher(formatPattern);
		while (matcher.find()) {
			String pattern = matcher.group(1);
			if (pattern.startsWith("$TODAY")) {
				Matcher matcher1 = Pattern.compile("\\((.*?)\\)").matcher(pattern);
				if (matcher1.find()) {
					for (String dateFormat : matcher1.group(1).split("\\%t")) {
						if (dateFormat.isEmpty()) { continue; }
						formattedSeq.append(String.format("%t" + dateFormat, new Date()));
					}
				}
			} else if (pattern.startsWith("$SEQ")) {
				System.out.println(pattern);
				Matcher matcher1 = Pattern.compile("\\((.*?)\\)").matcher(pattern);
				if (matcher1.find()) {
					System.out.println(matcher1.group());
					formattedSeq.append(String.format(matcher1.group(1), nextSeq));
				}
			} else {
				formattedSeq.append(pattern);
			}
		}

		return formattedSeq.toString();
	}


}
