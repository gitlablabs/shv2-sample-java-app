package sg.tech.testlab.service;

import java.util.List;

import sg.tech.testlab.model.Code;

public interface CodeService {
	public Code getCodeById(Long id);

	public List<Code> findByCategory(String category);
	
	public List<Code> findOrderedListByCategory(String category);
}
