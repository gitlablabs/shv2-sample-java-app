package sg.tech.testlab.service;

public interface SequenceService {
	public String getNextFormattedSequence(String key);
}
