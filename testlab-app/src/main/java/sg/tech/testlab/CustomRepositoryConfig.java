package sg.tech.testlab;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import sg.tech.testlab.repository.SearchRepository;
import sg.tech.testlab.repository.nativeimpl.SearchRepositoryMySqlImpl;

@Configuration
public class CustomRepositoryConfig {
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;

	@Bean
	public SearchRepository searchRepository() {
		return new SearchRepositoryMySqlImpl(this.dataSource);
	}
}
