package sg.tech.testlab.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatusController {

	@RequestMapping("/engine-status")
	public String engineStatus() {
		return "alive";
	}

}
