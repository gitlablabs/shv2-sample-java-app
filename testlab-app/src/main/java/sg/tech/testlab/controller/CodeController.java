package sg.tech.testlab.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sg.tech.testlab.model.Code;
import sg.tech.testlab.service.CodeService;

@RestController
@RequestMapping(value = "/code")
public class CodeController {
	@Autowired
	private CodeService codeService;

	@RequestMapping("/get/{id}")
	public Code getById(@PathVariable("id") Long id) {
		return this.codeService.getCodeById(id);
	}

	@RequestMapping("/listByCategory/{category}")
	public List<Code> listByCategory(@PathVariable("category") String category) {
		return this.codeService.findOrderedListByCategory(category);
	}

}
