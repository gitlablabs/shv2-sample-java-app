package sg.tech.testlab.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sg.tech.testlab.model.ApplicationType;
import sg.tech.testlab.service.ApplicationService;

@RestController
@RequestMapping(value = "/applnType")
public class ApplicationTypeController {
	@Autowired
	private ApplicationService applicationService;

	@RequestMapping("/list")
	public List<ApplicationType> listAll() {
		return this.applicationService.findAllApplicationType();
	}

	@RequestMapping("/get/{id}")
	public ApplicationType getById(@PathVariable("id") Long id) {
		return this.applicationService.getApplicationTypeById(id);
	}

}
