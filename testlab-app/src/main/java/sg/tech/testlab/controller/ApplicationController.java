package sg.tech.testlab.controller;

import java.security.Principal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sg.tech.testlab.aaa.model.Role;
import sg.tech.testlab.aaa.model.User;
import sg.tech.testlab.aaa.service.UserService;
import sg.tech.testlab.model.Application;
import sg.tech.testlab.service.ApplicationService;
import sg.tech.testlab.service.CodeService;

@RestController
@RequestMapping(value = "/appln")
public class ApplicationController {
	private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private CodeService codeService;

	@Autowired
	private ApplicationService applicationService;

	@SuppressWarnings("unchecked")
	@RequestMapping("/submit")
	public Application submitApplication(@RequestBody Map<Object, Object> requestBody, HttpServletRequest request) throws ParseException {
		logger.debug("In submit");
		logger.debug(requestBody.toString());

		request.getParameterMap().entrySet().stream().forEach(paramEntry -> {
			logger.debug(paramEntry.toString());
		});

		Map<Object, Object> applicationDetails = (Map<Object, Object>) requestBody.get("application");
		Map<Object, Object> applicantDetails = (Map<Object, Object>) requestBody.get("applicant");

		Long applicationTypeCd = parseLong((String) applicationDetails.get("applicationTypeCd"), true);

		String appltName = (String) applicantDetails.get("name");
		String appltNric = (String) applicantDetails.get("nric");
		Long appltDobTimestamp = (Long) applicantDetails.get("dobTimestamp");
		Date appltDob = null;
		if (appltDobTimestamp != null && appltDobTimestamp != 0L) {
			appltDob = new Date(appltDobTimestamp);
		}

		
		Long appltGenderCd = parseLong((String) applicantDetails.get("genderCd"), false);
		Long appltBirthCountryCd = parseLong((String) applicantDetails.get("birthCountryCd"), false);
		Long appltBirthStateCd = parseLong((String) applicantDetails.get("birthStateCd"), false);
		Long appltRaceCd = parseLong((String) applicantDetails.get("raceCd"), false);
		Long appltReligionCd = parseLong((String) applicantDetails.get("religionCd"), false);

		List<Long> languageCdList = new ArrayList<Long>();
		Map<Object, Object> applicantLanguageMap = (Map<Object, Object>) applicantDetails.get("language");
		if (applicantLanguageMap != null) {
			applicantLanguageMap.entrySet().stream().forEach(langEntry -> {
				if (((boolean) langEntry.getValue()) == true) {
					languageCdList.add(parseLong((String) langEntry.getKey(), true));
				}
			});
		}
		
		Application application = this.applicationService.createApplication(applicationTypeCd, appltName, appltNric, appltDob, appltGenderCd, appltBirthCountryCd, appltBirthStateCd, appltRaceCd, appltReligionCd, languageCdList);

		return application;
	}

	@RequestMapping("/get/{id}")
	public Application getById(@PathVariable("id") Long id) {
		Application application = this.applicationService.getApplicationById(id);
		logger.debug(application.toString());
		return application;
	}

	@RequestMapping(path="/withdraw/{id}", method=RequestMethod.POST)
	public Map<Object, Object> withdraw(@PathVariable("id") Long id) {
		this.applicationService.withdrawApplication(id);
		Map<Object, Object> responseMap = new HashMap<Object, Object>();
		responseMap.put("success", true);
		return responseMap;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/searchByUser")
	public Page<Application> searchByUser(@RequestBody Map<Object, Object> requestBody, Principal principal) throws Exception {
		String userName = principal.getName();
		if (userName == null || userName.isEmpty()) {
			throw new RuntimeException("Not Authorized.");
		}
		User user = this.userService.getUserByUserName(userName);

		Map<Object, Object> criteriaMap = (Map<Object, Object>) requestBody.get("criteria");
		Map<Object, Object> pageMap = (Map<Object, Object>) requestBody.get("page");
		Map<Object, Object> sortMap = (Map<Object, Object>) requestBody.get("sorting");

		Long applicationTypecd = null, applicationStatusCd = null;
		String applicationRefNo = null, applicantName = null, applicantNric = null;

		if (criteriaMap != null) {
			String applicationTypeCdStr = (String) criteriaMap.get("applicationTypeCd");
			if (applicationTypeCdStr != null && !applicationTypeCdStr.isEmpty()) {
				applicationTypecd = new Long(applicationTypeCdStr.trim());
			}

			applicationRefNo = (String) criteriaMap.get("applicationRefNo");
			applicantName = (String) criteriaMap.get("name");
			applicantNric = (String) criteriaMap.get("nric");

			String applicationStatusCdStr = (String) criteriaMap.get("applicationStatusCd");
			if (applicationStatusCdStr != null && !applicationStatusCdStr.isEmpty()) {
				applicationStatusCd = new Long(applicationStatusCdStr.trim());
			}
		}

		PageRequest pageRequest = new PageRequest(((Integer)pageMap.get("pageNo"))-1, (Integer)pageMap.get("limit"));

		return this.applicationService.searchByUser(pageRequest, user, applicationRefNo, applicationTypecd, applicantName, applicantNric, applicationStatusCd, sortMap);
	}

	public static Long parseLong(String o, boolean mandatory) {
		Long val = null;
		try {
			val = new Long(o);
			return val;
		} catch (Exception e) {
			if (mandatory) {
				throw e;
			} else {
				return null;
			}
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/listForPorcessing")
	public Page<Application> listApplicationForPo(@RequestBody Map<Object, Object> requestBody, Principal principal) throws Exception {
		String userName = principal.getName();
		if (userName == null || userName.isEmpty()) {
			throw new RuntimeException("Not Authorized.");
		}
		User user = this.userService.getUserByUserName(userName);
		boolean hasPo = false;
		for (Role role : user.getRoles()) {
			if ("PROCESSING_USER".equals(role.getName())) {
				hasPo = true;
				break;
			}
		}
		if (!hasPo) {
			throw new RuntimeException("Not Authorized.");
		}

		Map<Object, Object> pageMap = (Map<Object, Object>) requestBody.get("page");
		Map<Object, Object> sortMap = (Map<Object, Object>) requestBody.get("sorting");

		List<Sort.Order> orderList = new ArrayList<Sort.Order>();
		for (Entry<Object, Object> sortEntry : sortMap.entrySet()) {
			orderList.add(new Sort.Order("asc".equalsIgnoreCase((String) sortEntry.getValue()) ? Direction.ASC : Direction.DESC, (String) sortEntry.getKey()));
		}
		
		PageRequest pageRequest = new PageRequest(((Integer)pageMap.get("pageNo"))-1, (Integer)pageMap.get("limit"), new Sort(orderList));

		return this.applicationService.findByStatus(pageRequest, this.codeService.getCodeById(6001L));
	}


	@RequestMapping(path="/approve/{id}", method=RequestMethod.POST)
	public Map<Object, Object> approve(@PathVariable("id") Long id) {
		this.applicationService.approveApplication(id);
		Map<Object, Object> responseMap = new HashMap<Object, Object>();
		responseMap.put("success", true);
		return responseMap;
	}

	@RequestMapping(path="/reject/{id}", method=RequestMethod.POST)
	public Map<Object, Object> reject(@PathVariable("id") Long id) {
		this.applicationService.rejectApplication(id);
		Map<Object, Object> responseMap = new HashMap<Object, Object>();
		responseMap.put("success", true);
		return responseMap;
	}
}
