package sg.tech.testlab.aaa.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sg.tech.testlab.aaa.model.User;
import sg.tech.testlab.aaa.service.UserService;

@RestController
@RequestMapping(value = "/auth")
public class AuthenticationController {
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

	@Autowired
	private UserService userService;

	@RequestMapping("/signup")
	public User signupUser(@RequestBody(required=true) User user) {
		try {
			if (logger.isTraceEnabled()) {
				logger.trace("signupUser|Begin...");
				logger.trace("signupUser|userDTO=[" + user.toString() + "]");
			}

			User existingUser = this.userService.getUserByUserName(user.getUserName().trim().toUpperCase());
			if (existingUser != null) {
//				return new ResponseEntity<UserDTO>()
			}

			
			// this.userService.createUser(name, userName, password, roles)


			return null;
		} finally {
			if (logger.isTraceEnabled()) {
				logger.trace("signupUser|End.");
			}
		}
	}
}
