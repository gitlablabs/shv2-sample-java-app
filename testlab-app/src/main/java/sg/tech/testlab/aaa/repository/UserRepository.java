package sg.tech.testlab.aaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sg.tech.testlab.aaa.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	public User getByUserName(String userName);
}
