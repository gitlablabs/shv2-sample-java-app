package sg.tech.testlab.aaa.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import sg.tech.testlab.aaa.model.User;
import sg.tech.testlab.aaa.repository.UserRepository;
import sg.tech.testlab.aaa.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	public UserRepository userRepository;

	@Override
	public User getUser(Long userId) {
		if (userId == null) {
			throw new IllegalArgumentException("userId is required");
		}

		return this.userRepository.findOne(userId);
	}

	@Override
	public User getUserByUserName(String userName) {
		if (StringUtils.isEmpty(userName) || userName.trim().isEmpty()) {
			throw new IllegalArgumentException("userName is required");
		}

		return this.userRepository.getByUserName(userName);
	}

}
