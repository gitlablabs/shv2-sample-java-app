package sg.tech.testlab.aaa.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "AAA_USER")
public class User implements Serializable {
	private static final long serialVersionUID = -750031524900100457L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USERID")
	private Long id;

	@Column(name = "USERNAME")
	private String userName;

	@Column(name = "PASSWORD_")
	private String password;

	@Column(name = "NAME_")
	private String name;

	@Column(name = "EMAIL_")
	private String email;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "AAA_USER_ROLE",
				joinColumns = @JoinColumn(name = "USERID", referencedColumnName = "USERID"),
				inverseJoinColumns = @JoinColumn(name = "ROLEID", referencedColumnName = "ROLEID"))
	private List<Role> roles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", password=" + password + ", name=" + name + ", email="
				+ email + ", roles=" + roles + "]";
	}
}
