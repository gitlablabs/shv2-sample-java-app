package sg.tech.testlab.aaa.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sg.tech.testlab.aaa.model.User;
import sg.tech.testlab.aaa.service.UserService;

@RestController
@RequestMapping(value = "/user")
public class UserController {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping("/get/{userId}")
	public User getUserById(@PathVariable(name = "userId", required = true) Long userId) {
		return this.userService.getUser(userId);
	}

	@RequestMapping("/getByUserName/{userName}")
	public User getUserByUserName(@PathVariable("userName") String userName) {
		return this.userService.getUserByUserName(userName);
	}
}
