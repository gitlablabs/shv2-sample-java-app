package sg.tech.testlab.aaa.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import sg.tech.testlab.aaa.model.User;
import sg.tech.testlab.aaa.service.UserService;

public class TokenAuthenticationService {
	static final long EXPIRATIONTIME = 864_000_000; // 10 days
	static final String SECRET = "ThisIsASecret";
	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER_STRING = "Authorization";

	static void addAuthentication(HttpServletResponse res, Authentication auth) throws IOException {
		String JWT = Jwts.builder()
						.setSubject(auth.getName())
						.claim("GrantedAuthorities", auth.getAuthorities())
						.setIssuedAt(new Date())
						.setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
						.signWith(SignatureAlgorithm.HS512, SECRET)
						.compact();

		HashMap<String, String> responseMap = new HashMap<String, String>();
		responseMap.put("token", JWT);
		// res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
		res.setContentType("application/json;charset=UTF-8");
		res.getOutputStream().print(new ObjectMapper().writeValueAsString(responseMap));
	}

	static Authentication getAuthentication(HttpServletRequest request, UserService userService) {
		String token = request.getHeader(HEADER_STRING);

		if (token == null || token.isEmpty()) {
			return null;
		}

		String userName = Jwts.parser().setSigningKey(SECRET)
								.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
								.getBody().getSubject();

		if (userName == null || userName.isEmpty()) {
			return null;
		}
		
		User user = userService.getUserByUserName(userName);
		if (user == null) {
			return null;
		}

		List<GrantedAuthority> grantedAuths = new ArrayList<>();
		user.getRoles().parallelStream().forEach(role -> {
			grantedAuths.add(new SimpleGrantedAuthority(role.getName()));
		});

		return new UsernamePasswordAuthenticationToken(userName, null, grantedAuths);
	}
}
