package sg.tech.testlab.aaa.service;

import sg.tech.testlab.aaa.model.User;

public interface UserService {
	public User getUser(Long userId);
	public User getUserByUserName(String userName);
}
