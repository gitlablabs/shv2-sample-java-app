package sg.tech.testlab.aaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sg.tech.testlab.aaa.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	public Role getByName(String name);
}
