package sg.tech.testlab.aaa.provider;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import sg.tech.testlab.aaa.model.User;
import sg.tech.testlab.aaa.service.UserService;

@Component
public class DbUsernamePasswordAuthenticationProvider implements AuthenticationProvider {
	private static final Logger logger = LoggerFactory.getLogger(DbUsernamePasswordAuthenticationProvider.class);

	@Autowired
	@Qualifier("aaaPasswordEncoder")
	private PasswordEncoder aaaPasswordEncoder;

	@Autowired
	private UserService userService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		if (logger.isTraceEnabled()) {
			logger.trace("authenticate|Begin...");
		}

		try {
			if (logger.isDebugEnabled()) {
				logger.debug("authenticate|authentication=[" + authentication + "]");
			}

			User user = userService.getUserByUserName(authentication.getName());

			if (user == null) {
				throw new BadCredentialsException("Invalid User Name/ Password");
			}

			if (!this.aaaPasswordEncoder.matches(authentication.getCredentials().toString(), user.getPassword())) {
				throw new BadCredentialsException("Invalid User Name/ Password");
			}

			List<GrantedAuthority> grantedAuths = new ArrayList<>();
			user.getRoles().stream().forEach(role -> {
				grantedAuths.add(new SimpleGrantedAuthority(role.getName()));
			});

			return new UsernamePasswordAuthenticationToken(authentication.getName(), authentication.getCredentials(),
					grantedAuths);
		} finally {
			if (logger.isTraceEnabled()) {
				logger.trace("authenticate|End.");
			}
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}
}
