package sg.tech.testlab;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration
@EntityScan(basePackages = {"sg.tech.testlab"})
@EnableJpaRepositories(basePackages = {"sg.tech.testlab"})
public class TestlabApplicationV1 {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(TestlabApplicationV1.class);

	public static void main(String[] args) {
		SpringApplication.run(TestlabApplicationV1.class, args);
	}
}
