package sg.tech.testlab.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import sg.tech.testlab.aaa.model.User;

@Entity
@Table(name = "JOB_MASTER")
public class Job implements Serializable {
	private static final long serialVersionUID = 6920297445128462073L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "JOBID")
	private Long id;

	@Column(name = "TITLE_")
	private String title;

	@Column(name = "DESC_")
	private String description;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CREATEBY")
	public User createdBy;

	@Column(name = "CREATEON")
	public Date createdOn;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "UPDATEBY")
	public User updatedBy;

	@Column(name = "UPDATEON")
	public Date updatedOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
}
