package sg.tech.testlab.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CORE_SEQUENCE")
public class Sequence implements Serializable {
	private static final long serialVersionUID = 6627092158589209215L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SEQUENCEID")
	private Long id;

	@Column(name = "KEY_")
	private String key;

	@Column(name = "LASTSEQ_")
	private Long lastSeq;

	@Column(name = "PATTERN_")
	private String pattern;

	@Column(name = "MAX_")
	private Long max;

	@Column(name = "CYCLE_")
	private boolean cycle;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Long getLastSeq() {
		return lastSeq;
	}

	public void setLastSeq(Long lastSeq) {
		this.lastSeq = lastSeq;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public Long getMax() {
		return max;
	}

	public void setMax(Long max) {
		this.max = max;
	}

	public boolean isCycle() {
		return cycle;
	}

	public void setCycle(boolean cycle) {
		this.cycle = cycle;
	}

	@Override
	public String toString() {
		return "Sequence [id=" + id + ", key=" + key + ", lastSeq=" + lastSeq + ", pattern=" + pattern + ", max=" + max
				+ ", cycle=" + cycle + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (cycle ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((lastSeq == null) ? 0 : lastSeq.hashCode());
		result = prime * result + ((max == null) ? 0 : max.hashCode());
		result = prime * result + ((pattern == null) ? 0 : pattern.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sequence other = (Sequence) obj;
		if (cycle != other.cycle)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (lastSeq == null) {
			if (other.lastSeq != null)
				return false;
		} else if (!lastSeq.equals(other.lastSeq))
			return false;
		if (max == null) {
			if (other.max != null)
				return false;
		} else if (!max.equals(other.max))
			return false;
		if (pattern == null) {
			if (other.pattern != null)
				return false;
		} else if (!pattern.equals(other.pattern))
			return false;
		return true;
	}
}
