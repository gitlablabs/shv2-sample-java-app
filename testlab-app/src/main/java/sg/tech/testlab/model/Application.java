package sg.tech.testlab.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import sg.tech.testlab.aaa.model.User;

@Entity
@Table(name = "APPLN_APPLICATION")
public class Application implements Serializable {
	private static final long serialVersionUID = 4539254489203491735L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "APPLICATIONID")
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="APPLNTYPEID")
	private ApplicationType applicationType;

	@Column(name = "APPLN_REF_NO")
	private String applicationRefNo;

	@Column(name = "APPLT_NAME")
	private String applicantName;

	@Column(name = "APPLT_NRIC")
	private String applicantNric;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="APPLT_GENDER_CD")
	private Code applicantGender;

	@Column(name = "APPLT_DOB")
	private Date applicantDob;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="APPLT_BIRTH_COUNTRY_CD")
	private Code applicantBirthCountry;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="APPLT_BIRTH_STATE_CD")
	private Code applicantBirthState;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="APPLT_RACE_CD")
	private Code applicantRace;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="APPLT_RELIGION_CD")
	private Code applicantReligion;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="STATUSCD")
	private Code status;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CREATEBY")
	private User createdBy;

	@Column(name = "CREATEON")
	private Date createdOn;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="UPDATEBY")
	private User updatedBy;

	@Column(name = "UPDATEON")
	private Date updatedOn;

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "APPLN_APPLICATION_APPLT_LANGUAGE",
				joinColumns = @JoinColumn(name= "APPLICATIONID", referencedColumnName = "APPLICATIONID"),
				inverseJoinColumns = @JoinColumn(name= "LANGUAGECD", referencedColumnName = "CODEID"))
	private List<Code> applicantLanguages;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ApplicationType getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(ApplicationType applicationType) {
		this.applicationType = applicationType;
	}

	public String getApplicationRefNo() {
		return applicationRefNo;
	}

	public void setApplicationRefNo(String applicationRefNo) {
		this.applicationRefNo = applicationRefNo;
	}

	public String getApplicantName() {
		return applicantName;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	public String getApplicantNric() {
		return applicantNric;
	}

	public void setApplicantNric(String applicantNric) {
		this.applicantNric = applicantNric;
	}

	public Code getApplicantGender() {
		return applicantGender;
	}

	public void setApplicantGender(Code applicantGender) {
		this.applicantGender = applicantGender;
	}

	public Date getApplicantDob() {
		return applicantDob;
	}

	public void setApplicantDob(Date applicantDob) {
		this.applicantDob = applicantDob;
	}

	public Code getApplicantBirthCountry() {
		return applicantBirthCountry;
	}

	public void setApplicantBirthCountry(Code applicantBirthCountry) {
		this.applicantBirthCountry = applicantBirthCountry;
	}

	public Code getApplicantRace() {
		return applicantRace;
	}

	public void setApplicantRace(Code applicantRace) {
		this.applicantRace = applicantRace;
	}

	public Code getApplicantReligion() {
		return applicantReligion;
	}

	public void setApplicantReligion(Code applicantReligion) {
		this.applicantReligion = applicantReligion;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public List<Code> getApplicantLanguages() {
		return applicantLanguages;
	}

	public void setApplicantLanguages(List<Code> applicantLanguages) {
		this.applicantLanguages = applicantLanguages;
	}

	public Code getApplicantBirthState() {
		return applicantBirthState;
	}

	public void setApplicantBirthState(Code applicantBirthState) {
		this.applicantBirthState = applicantBirthState;
	}

	public Code getStatus() {
		return status;
	}

	public void setStatus(Code status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Application [id=" + id + ", applicationType=" + applicationType + ", applicationRefNo="
				+ applicationRefNo + ", applicantName=" + applicantName + ", applicantNric=" + applicantNric
				+ ", applicantGender=" + applicantGender + ", applicantDob=" + applicantDob + ", applicantBirthCountry="
				+ applicantBirthCountry + ", applicantBirthState=" + applicantBirthState + ", applicantRace="
				+ applicantRace + ", applicantReligion=" + applicantReligion + ", status=" + status + ", createdBy="
				+ createdBy + ", createdOn=" + createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn
				+ ", applicantLanguages=" + applicantLanguages + "]";
	}

	
}
