var projectName = 'testlab-web';

var buildBaseDir = './build';
var buildBasePackageSrcDir = buildBaseDir + '/.tmp';
var buildBasePackageUglifiedSrcDir = buildBaseDir + '/.tmp-final';

var buildConfig = {
	bowerComponentsBaseDir : './bower_components',
	externalLib : {
		jsName : 'vendor.js',
		cssName : 'vendor.css',
		packageSrcBaseDir : buildBasePackageSrcDir + '/lib/vendor',
		packageTargetBaseDirRelativePath : 'lib/vendor'
	},
	appPackage : {
		intranet : 'intranet',
		internet : 'internet',
		public : 'public'
	},
	dist : {
		dev : buildBaseDir + '/dist/dev',
		sit : buildBaseDir + '/dist/sit'
	}
};

var gulp = require('gulp');
var runSequence = require('run-sequence');
var plugins = require('gulp-load-plugins')({
	pattern : [ 'gulp-*', 'gulp.*', 'main-bower-files' ],
	replaceString : /\bgulp[\-.]/
});
var streamqueue = require('streamqueue');
var mergeStream = require('merge-stream');

gulp.task('clean', function() {
	return gulp.src(buildBaseDir).pipe(plugins.clean());
});

gulp.task('external-lib:package:bower-js-resource', function() {
	var filterJS = plugins.filter('**/*.js', { restore : true });
	return gulp.src('./bower.json')
		.pipe(plugins.mainBowerFiles({
			dependencies : {
				'angular-bootstrap' : 'angular'
			},
			overrides : {
				'bootstrap-less' : { ignore : true },
				'font-awesome' : { ignore : true },
				'lessfonts-open-sans' : { ignore : true },
				'ngtable-bower' : { "main": ["./ng-table.js" ] },
				'angular-ui-bootstrap-datetimepicker' : { "main": ["./datetimepicker.js" ] }
			}
		}))
		.pipe(filterJS)
		.pipe(plugins.concat(buildConfig.externalLib.jsName))
		.pipe(filterJS.restore).pipe(gulp.dest(buildConfig.externalLib.packageSrcBaseDir));
});

gulp.task('external-lib:prepare-resource:bower-css-resource', function() {
	var copyBootstrapStream = gulp.src(buildConfig.bowerComponentsBaseDir + '/bootstrap-less/fonts/**.*').pipe(gulp.dest(buildConfig.externalLib.packageSrcBaseDir + '/resources/fonts/bootstrap'));
	var copyFontAwesomeStream = gulp.src(buildConfig.bowerComponentsBaseDir + '/font-awesome/fonts/**.*').pipe(gulp.dest(buildConfig.externalLib.packageSrcBaseDir + '/resources/fonts/font-awesome'));
	var copyOpenSansStream = gulp.src(buildConfig.bowerComponentsBaseDir + '/lessfonts-open-sans/dist/fonts/OpenSans/**').pipe(gulp.dest(buildConfig.externalLib.packageSrcBaseDir + '/resources/fonts/open-sans'));

	return mergeStream(copyBootstrapStream, copyFontAwesomeStream, copyOpenSansStream);
});

gulp.task('external-lib:package:bower-css-resource', ['external-lib:prepare-resource:bower-css-resource'], function() {
	return streamqueue({ objectMode: true},
			gulp.src(['vendor/vendor.less']).pipe(plugins.less({
				paths : [
					buildConfig.bowerComponentsBaseDir + '/bootstrap-less/less',
					buildConfig.bowerComponentsBaseDir + '/font-awesome/less',
					buildConfig.bowerComponentsBaseDir + '/lessfonts-open-sans/src/less'
				]
			})),
			gulp.src([
				buildConfig.bowerComponentsBaseDir + '/ngtable-bower/ng-table.css',
				buildConfig.bowerComponentsBaseDir + '/angular-ui-bootstrap-datetimepicker/datetimepicker.css'
				// buildConfig.bowerComponentsBaseDir + '/angular-material/angular-material.css'
			])
		)
		.pipe(plugins.concat(buildConfig.externalLib.cssName))
		.pipe(gulp.dest(buildConfig.externalLib.packageSrcBaseDir));
});

gulp.task('external-lib:package', [ 'external-lib:package:bower-js-resource', 'external-lib:package:bower-css-resource' ]);






gulp.task('build:dev:intranet', [ 'external-lib:package' ], function() {
	var externalLibStream = gulp.src(buildConfig.externalLib.packageSrcBaseDir + '/**')
		.pipe(gulp.dest(buildConfig.dist.dev + '/' + buildConfig.appPackage.intranet + '/' + buildConfig.externalLib.packageTargetBaseDirRelativePath));

	var copyIntranetResourceStream = gulp.src([
			'./' + buildConfig.appPackage.intranet + '/**',
			'!./' + buildConfig.appPackage.intranet + '/{scripts,scripts/**}',
		])
		.pipe(gulp.dest(buildConfig.dist.dev + '/' + buildConfig.appPackage.intranet));

	var buildAppJsStream = streamqueue({ objectMode: true},
				gulp.src(['./' + buildConfig.appPackage.intranet + '/scripts/app.js']),
				gulp.src(['./' + buildConfig.appPackage.intranet + '/scripts/app-router.js']),
				gulp.src(['./' + buildConfig.appPackage.intranet + '/scripts/directives/**']),
				gulp.src(['./' + buildConfig.appPackage.intranet + '/scripts/services/**']),
				gulp.src(['./' + buildConfig.appPackage.intranet + '/scripts/controllers/**'])
			)
			.pipe(plugins.concat('app.js'))
			.pipe(gulp.dest(buildConfig.dist.dev + '/' + buildConfig.appPackage.intranet + '/scripts'));

	var copyAppApiConfig = gulp.src('./' + buildConfig.appPackage.intranet + '/scripts/app-api-config.js')
		.pipe(gulp.dest(buildConfig.dist.dev + '/' + buildConfig.appPackage.intranet + '/scripts'));

	return mergeStream(externalLibStream, copyIntranetResourceStream, buildAppJsStream, copyAppApiConfig);
});

gulp.task('build:dev:internet', [ 'external-lib:package' ], function() {
	var externalLibStream = gulp.src(buildConfig.externalLib.packageSrcBaseDir + '/**')
		.pipe(gulp.dest(buildConfig.dist.dev + '/' + buildConfig.appPackage.internet + '/' + buildConfig.externalLib.packageTargetBaseDirRelativePath));

		var copyInternetResourceStream = gulp.src([
				'./' + buildConfig.appPackage.internet + '/**',
				'!./' + buildConfig.appPackage.internet + '/{scripts,scripts/**}',
			])
			.pipe(gulp.dest(buildConfig.dist.dev + '/' + buildConfig.appPackage.internet));


	return mergeStream(externalLibStream, copyInternetResourceStream);
});

gulp.task('serve:dev:intranet:webserver', function() {
	return plugins.connect.server({
		livereload : true,
		port: 8181,
		root : [ buildConfig.dist.dev + '/' + buildConfig.appPackage.intranet ]
	});
});

gulp.task('serve:dev:intranet:watch', function() {
	return gulp.watch('./' + buildConfig.appPackage.intranet + '/**', ['build:dev:intranet']);
});
// gulp.task('serve:dev:intranet', [ 'build:dev:intranet', 'serve:dev:intranet:webserver' ]);
gulp.task('serve:dev:intranet', [ 'build:dev:intranet', 'serve:dev:intranet:webserver', 'serve:dev:intranet:watch' ]);


gulp.task('serve-public-dev', function() {
	plugins.connect.server({
		livereload : true,
		// root : [ 'src', buildBasePackageSrcDir ]
		root : [ 'public', buildBasePackageSrcDir ]
		// root : [ 'src1' ]
	});
});




gulp.task('build:sit:intranet', [ 'external-lib:package' ], function() {
	var copyResourceStream = gulp.src(buildConfig.externalLib.packageSrcBaseDir + '/resources/**')
		.pipe(gulp.dest(buildConfig.dist.sit + '/' + buildConfig.appPackage.intranet + '/' + buildConfig.externalLib.packageTargetBaseDirRelativePath + '/resources'));

	var minifyJsStream = gulp.src(buildConfig.externalLib.packageSrcBaseDir + '/vendor.js')
		.pipe(plugins.uglify())
		.pipe(gulp.dest(buildConfig.dist.sit + '/' +  buildConfig.appPackage.intranet + '/' + buildConfig.externalLib.packageTargetBaseDirRelativePath));

	var minifyCssStream = gulp.src(buildConfig.externalLib.packageSrcBaseDir + '/vendor.css')
		.pipe(plugins.uglifycss())
		.pipe(gulp.dest(buildConfig.dist.sit + '/' +  buildConfig.appPackage.intranet + '/' + buildConfig.externalLib.packageTargetBaseDirRelativePath));

	return mergeStream(copyResourceStream, minifyJsStream, minifyCssStream);
});

gulp.task('build:sit:internet', [ 'external-lib:package' ], function() {
	var copyResourceStream = gulp.src(buildConfig.externalLib.packageSrcBaseDir + '/resources/**')
		.pipe(gulp.dest(buildConfig.dist.sit + '/' + buildConfig.appPackage.internet + '/' + buildConfig.externalLib.packageTargetBaseDirRelativePath + '/resources'));

	var minifyJsStream = gulp.src(buildConfig.externalLib.packageSrcBaseDir + '/vendor.js')
		.pipe(plugins.uglify())
		.pipe(gulp.dest(buildConfig.dist.sit + '/' +  buildConfig.appPackage.internet + '/' + buildConfig.externalLib.packageTargetBaseDirRelativePath));

	var minifyCssStream = gulp.src(buildConfig.externalLib.packageSrcBaseDir + '/vendor.css')
		.pipe(plugins.uglifycss())
		.pipe(gulp.dest(buildConfig.dist.sit + '/' +  buildConfig.appPackage.internet + '/' + buildConfig.externalLib.packageTargetBaseDirRelativePath));

	return mergeStream(copyResourceStream, minifyJsStream, minifyCssStream);
});

gulp.task('build:sit', [ 'build:sit:intranet', 'build:sit:internet' ]);


gulp.task('build:sit:public', [ 'external-lib:package' ], function() {
	var copyResourceStream = gulp.src(buildConfig.externalLib.packageSrcBaseDir + '/resources/**')
		.pipe(gulp.dest(buildConfig.dist.sit + '/' + buildConfig.appPackage.public + '/' + buildConfig.externalLib.packageTargetBaseDirRelativePath + '/resources'));

	var minifyJsStream = gulp.src(buildConfig.externalLib.packageSrcBaseDir + '/vendor.js')
		// .pipe(plugins.uglify())
		.pipe(gulp.dest(buildConfig.dist.sit + '/' +  buildConfig.appPackage.public + '/' + buildConfig.externalLib.packageTargetBaseDirRelativePath));

	var minifyCssStream = gulp.src(buildConfig.externalLib.packageSrcBaseDir + '/vendor.css')
		// .pipe(plugins.uglifycss())
		.pipe(gulp.dest(buildConfig.dist.sit + '/' +  buildConfig.appPackage.public + '/' + buildConfig.externalLib.packageTargetBaseDirRelativePath));

	var copyPublicResourceStream = gulp.src([
			'./' + buildConfig.appPackage.public + '/**',
			'!./' + buildConfig.appPackage.public + '/{scripts,scripts/**}',
			'!./' + buildConfig.appPackage.public + '/{css,css/**}'
		])
		.pipe(gulp.dest(buildConfig.dist.sit + '/' + buildConfig.appPackage.public));

	var buildAppJsStream = streamqueue({ objectMode: true},
			gulp.src(['./' + buildConfig.appPackage.public + '/scripts/app.js']),
			gulp.src(['./' + buildConfig.appPackage.public + '/scripts/app-router.js']),
			gulp.src(['./' + buildConfig.appPackage.public + '/scripts/directives/**']),
			gulp.src(['./' + buildConfig.appPackage.public + '/scripts/services/**']),
			gulp.src(['./' + buildConfig.appPackage.public + '/scripts/controllers/**'])
		)
		.pipe(plugins.concat('app.js'))
		// .pipe(plugins.uglify())
		.pipe(gulp.dest(buildConfig.dist.sit + '/' + buildConfig.appPackage.public + '/scripts'));

	var minifyCssStream = gulp.src(buildConfig.appPackage.public + '/css/**')
		.pipe(plugins.concat('app.css'))
		// .pipe(plugins.uglifycss())
		.pipe(gulp.dest(buildConfig.dist.sit + '/' + buildConfig.appPackage.public + '/css'));

	return mergeStream(copyResourceStream, minifyJsStream, minifyCssStream, copyPublicResourceStream, buildAppJsStream, minifyCssStream);
});

gulp.task('serve:sit:public:webserver', function() {
	return plugins.connect.server({
		livereload : true,
		port: 8280,
		root : [ buildConfig.dist.sit + '/' + buildConfig.appPackage.public ]
	});
});



//gulp.task('prepare-app-resources:html', function() {
//	return gulp.src(conf.app.src.path + '/html/**')
//			.pipe(gulp.dest(conf.app.build.path));
//});
//
//gulp.task('prepare-app-resources', ['prepare-app-resources:html']);
//
//gulp.task('build:dev', ['prepare-bower-resources', 'prepare-app-resources'], function() {
//	var rootLibPath = conf.dist.dev + '/lib/' + projectName + '-root';
//	gulp.src(conf.rootAsset.srcPath + '/**').pipe(gulp.dest(rootLibPath));
//	return gulp.src(conf.app.build.path + '/**').pipe(gulp.dest(conf.dist.dev));
//});
//
//gulp.task('build:sit', ['prepare-bower-resources'], function() {
//	var rootLibPath = conf.dist.sit + '/lib/' + projectName + '-root';
//});
//
//gulp.task('watch:dev', function() {
//	return plugins.watch(conf.app.src.path + '/**', ['build:dev']);
//});
//
//
//gulp.task('serve:dev', ['build:dev'], function() {
//	return gulp.src(conf.dist.dev)
//			.pipe(plugins.webserver({
//				path: conf.dist.dev,
//				livereload: true,
//				directoryListing: true,
//				port: 8080,
//				fallback: 'index.html'
//			}));
//});
