var testlabApp = angular.module('testlabApp', [ 'ui.bootstrap', 'ui.router', 'satellizer', 'ngTable', 'ui.bootstrap.datetimepicker' ]);

testlabApp.constant('ApiResourceConfig', (function() {
	// var apiBaseUrl = 'http://172.26.18.240:8081/api/v1';
	// var apiBaseUrl = 'http://192.168.139.128:8281/public/api/v1';
	var apiBaseUrl = '/public/api/v1';
	return {
		BASE_URL : apiBaseUrl,
		LOGIN_URL : apiBaseUrl + '/authn/login',
		USER_BY_USERNAME : function(userName) {
			return apiBaseUrl + '/user/getByUserName/' + userName;
		},
		CODES_BY_CATEGORY : function(category) {
			return apiBaseUrl + '/code/listByCategory/' + category;
		},
		CODE_BY_ID : function(id) {
			return apiBaseUrl + '/code/get/' + id;
		},
		APPLN_TYPES : apiBaseUrl + '/applnType/list',
		APPLN_TYPE_BY_ID : function(id) {
			return apiBaseUrl + '/applnType/get/' + id;
		},
		APPLN_PUBLIC_SEARCH : apiBaseUrl + '/appln/searchByUser',
		APPLN_WITHDRAW : function(id) {
			return apiBaseUrl + '/appln/withdraw/' + id;
		},
		APPLN_PO_LIST : apiBaseUrl + '/appln/listForPorcessing',
		APPLN_BY_ID : function(id) {
			return apiBaseUrl + '/appln/get/' + id;
		},
		APPLN_APPROVE : function(id) {
			return apiBaseUrl + '/appln/approve/' + id;
		},
		APPLN_REJECT : function(id) {
			return apiBaseUrl + '/appln/reject/' + id;
		}
	}
})());

testlabApp.directive('inputValidationAlphabetOnly', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attr, ngModelCtrl) {
			function fromUser(text) {
				var transformedInput = text.replace(/[^a-zA-Z\s]/g, '');
				if (transformedInput !== text) {
					ngModelCtrl.$setViewValue(transformedInput);
					ngModelCtrl.$render();
				}
				return transformedInput;
			}
			ngModelCtrl.$parsers.push(fromUser);
		}
	};
});

testlabApp.directive('inputValidationAlphanumericOnly', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attr, ngModelCtrl) {
			function fromUser(text) {
				var transformedInput = text.replace(/[^a-zA-Z0-9]/g, '');
				if (transformedInput !== text) {
					ngModelCtrl.$setViewValue(transformedInput);
					ngModelCtrl.$render();
				}
				return transformedInput;
			}
			ngModelCtrl.$parsers.push(fromUser);
		}
	};
});



testlabApp.config(function($stateProvider, $urlRouterProvider, $authProvider, ApiResourceConfig) {
	 $authProvider.loginUrl = ApiResourceConfig.LOGIN_URL;
	 $authProvider.storageType = 'sessionStorage';

	var skipIfLoggedIn = [ '$q', '$auth',
		function($q, $auth) {
			var deferred = $q.defer();
			if ($auth.isAuthenticated()) {
				deferred.reject();
			} else {
				deferred.resolve();
			}
			return deferred.promise;
		}
	];

	var loginRequired = [ '$q', '$location', '$auth',
		function($q, $location, $auth) {
			var deferred = $q.defer();
			if ($auth.isAuthenticated()) {
				deferred.resolve();
			} else {
				$location.path('/login');
			}
			return deferred.promise;
		}
	];

	$stateProvider
		.state('home', {
			url : '/home',
			controller : 'Main.HomeCtrl',
			templateUrl : 'partials/home.tpl.html'
		})
		.state('profile', {
			url : '/profile',
			templateUrl : 'partials/profile.tpl.html',
			resolve : {
				loginRequired : loginRequired
			}
		})
		.state('login', {
			url : '/login',
	        controller : 'AAA.LoginCtrl',
			templateUrl : 'partials/login.tpl.html',
			resolve : {
				skipIfLoggedIn: skipIfLoggedIn
			}
		})
		.state('logout', {
			url : '/logout',
			controller : 'AAA.LogoutCtrl',
			templateUrl : null
		})
//		.state('applnApplyNew-old', {
//			url : '/applnApplyNew-old',
//	        controller : 'APPLN.ApplyNewApplnOldCtrl',
//			templateUrl : 'partials/appln/apply-new-main-old.tpl.html',
//			resolve : {
//				loginRequired : loginRequired
//			}
//		})
//		.state('applnApplyNewStep-old', {
//			url : '/applnApplyNew-old/:stepNo',
//	        controller : 'APPLN.ApplyNewApplnOldCtrl',
//			templateUrl : 'partials/appln/apply-new-main-old.tpl.html',
//			resolve : {
//				loginRequired : loginRequired
//			}
//		})
		.state('applnApplyNew', {
			abstract : true,
			url : '/applnApplyNew',
			controller : 'APPLN.ApplyNewApplnMainCtrl',
			templateUrl : 'partials/appln/apply-new-main.tpl.html',
			resolve : {
				loginRequired : loginRequired
			}
		})
		.state('applnApplyNew.1', {
			url : '/1',
			controller : 'APPLN.ApplyNewApplnStep1Ctrl',
			templateUrl : 'partials/appln/apply-new-step1.tpl.html',
			resolve : {
				loginRequired : loginRequired
			}
		})
		.state('applnApplyNew.2', {
			url : '/2',
			controller : 'APPLN.ApplyNewApplnStep2Ctrl',
			templateUrl : 'partials/appln/apply-new-step2.tpl.html',
			resolve : {
				loginRequired : loginRequired
			}
		})
		.state('applnApplyNew.3', {
			url : '/3',
			controller : 'APPLN.ApplyNewApplnStep3Ctrl',
			templateUrl : 'partials/appln/apply-new-step3.tpl.html',
			resolve : {
				loginRequired : loginRequired
			}
		})
		.state('applnApplyNew.4', {
			url : '/4',
			controller : 'APPLN.ApplyNewApplnStep4Ctrl',
			templateUrl : 'partials/appln/apply-new-step4.tpl.html',
			resolve : {
				loginRequired : loginRequired
			}
		})
		.state('applnEnquire', {
			url : '/applnEnquire',
			controller : 'APPLN.EnquireApplnCtrl',
			templateUrl : 'partials/appln/enquire-appln.html',
			resolve : {
				loginRequired : loginRequired
			}
		})
		.state('task', {
			abstract : true,
			url : '/task',
			controller : 'TASK.TaskMainCtrl',
			templateUrl : 'partials/task/task-main.tpl.html',
			resolve : {
				loginRequired : loginRequired
			}
		})
		.state('task.list', {
			url : '/list',
			controller : 'TASK.TaskListCtrl',
			templateUrl : 'partials/task/task-list.tpl.html',
			resolve : {
				loginRequired : loginRequired
			}
		})
		.state('task.process', {
			url : '/process/:applicationId',
			controller : 'TASK.TaskProcessCtrl',
			templateUrl : 'partials/task/task-detail.tpl.html',
			resolve : {
				loginRequired : loginRequired
			}
		})
		;

	$urlRouterProvider.otherwise('/home');

});

testlabApp.controller('Main.HomeCtrl', function($scope, $auth, $http, ApiResourceConfig) {
	$scope.data = $scope.data || {};
	if ($auth.isAuthenticated()) {
		$http({ method: 'GET', url: ApiResourceConfig.USER_BY_USERNAME($auth.getPayload().sub) }).then(
			function successCallback(response) {
				$scope.data.user = response.data;
			},
			function errorCallback(response) {
			}
		);
	}
});

testlabApp.controller('Main.NavbarCtrl', function($scope, $auth) {
	$scope.isAuthenticated = function() {
		return $auth.isAuthenticated();
	};

	$scope.isPO = function() {
		if ($auth.isAuthenticated()) {
			var grantedAuthorities = $auth.getPayload().GrantedAuthorities;
			for (var i=0; i<$auth.getPayload().GrantedAuthorities.length; i++) {
				if ($auth.getPayload().GrantedAuthorities[i].authority == 'PROCESSING_USER') {
					return true;
				}
			}
		}
		return false;
	};

	$scope.isPublic = function() {
		if ($auth.isAuthenticated()) {
			var grantedAuthorities = $auth.getPayload().GrantedAuthorities;
			for (var i=0; i<$auth.getPayload().GrantedAuthorities.length; i++) {
				if ($auth.getPayload().GrantedAuthorities[i].authority == 'ESERVICE_USER') {
					return true;
				}
			}
		}
		return false;
	};


//	if (!authorized) {
//		$location.path('/accessDenied');
//	}


// $scope.tree = [
// {
// name : 'Apply New Application',
// link : '/#!/profile'
// },
// {
// name : 'Enquire Application Status',
// link : '/#!/profile'
// }
// ];


// $scope.tree = [{
// name: "Bob",
// link: "#",
// subtree: [{
// name: "Ann",
// link: "#"
// }]
// }, {
// name: "Jon",
// link: "#",
// subtree: [{
// name: "Mary",
// link: "#"
// }]
// }, {
// name: "divider",
// link: "#"
// }, {
// name: "Another person",
// link: "#"
// }, {
// name: "divider",
// link: "#"
// },{
// name: "Again another person",
// link: "#"
// }];
});


// testlabApp.directive('tree', function() {
// return {
// restrict: "E",
// replace: true,
// scope: {
// tree: '='
// },
// templateUrl: 'template-ul.html'
// };
// });
//
// testlabApp.directive('leaf', function($compile) {
// return {
// restrict: "E",
// replace: true,
// scope: {
// leaf: "="
// },
// templateUrl: 'template-li.html',
// link: function(scope, element, attrs) {
// if (angular.isArray(scope.leaf.subtree)) {
// element.append("<tree tree='leaf.subtree'></tree>");
// element.addClass('dropdown-submenu');
// $compile(element.contents())(scope);
// } else {
// element.bind('click', function() {
// alert("You have clicked on " + scope.leaf.name);
// });
//
// }
// }
// };
// });


testlabApp.controller('AAA.LoginCtrl', function($scope, $location, $state, $auth, $http) {
	$scope.login = function() {
		$auth.login($scope.user)
			.then(
				function success(response) {
					$location.path('/#!/profile');
					$scope.error = false;
				}
			).catch(function(error) {
				$scope.error = true;
				$scope.errorMsg = error.data.message;
	        });
	}
});

testlabApp.controller('AAA.LogoutCtrl', function($location, $auth) {
	if (!$auth.isAuthenticated()) {
		$location.path('/');
	} else {
		$auth.logout()
		.then(function() {
			$location.path('/');
		});
	}
});

testlabApp.controller('APPLN.ApplyNewApplnMainCtrl', function($scope, $location, $state, $auth, $stateParams, $http, ApiResourceConfig) {
	$scope.data = {
		pageState : {
			currentStep : 1,
			step1Completed : false,
			step2Completed : false,
			step3Completed : false,
			step4Completed : false
		},
		applicant : {},
		application : {}
	}
});

testlabApp.controller('APPLN.ApplyNewApplnStep1Ctrl', function($scope, $location, $state, $auth, $stateParams, $http, ApiResourceConfig) {
	$scope.$parent.data.pageState.currentStep = 1;

	$scope.submit = function() {
		$scope.$parent.data.pageState.step1Completed = true;
		$state.go('applnApplyNew.2');
	}
	$scope.back = function(stepNo) {
		$location.path('#');
	}
});
testlabApp.controller('APPLN.ApplyNewApplnStep2Ctrl', function($scope, $location, $state, $auth, $stateParams, $http, ApiResourceConfig) {
	if ($scope.$parent.data.pageState.step1Completed == false) {
		$state.go('applnApplyNew.1');
		return;
	}

	$scope.applicantDobOptions = {
		maxDate: new Date()
	};

	$scope.$parent.data.pageState.currentStep = 2;

	$scope.data.application = $scope.$parent.data.application;
	$scope.data.applicant = $scope.$parent.data.applicant;

	$scope.submit = function() {
		if ($scope.backBtnClicked == true) {
			$scope.$parent.data.application = $scope.data.application;
			$scope.$parent.data.applicant = $scope.data.applicant;
			$state.go('applnApplyNew.1');
		} else {
			$scope.$parent.data.pageState.step2Completed = true;
			$scope.$parent.data.application = $scope.data.application;
			$scope.$parent.data.applicant = $scope.data.applicant;
			$state.go('applnApplyNew.3');
		}
	}

	$scope.updateState = function(country) {
		$scope.optionsData.stateOptions = [];
		$scope.data.applicant.birthStateCd = null;
		if (country && country != '') {
			$http({ method: 'GET', url: ApiResourceConfig.CODE_BY_ID(country) }).then(
				function successCallback(response) {
					$http({ method: 'GET', url: ApiResourceConfig.CODES_BY_CATEGORY('STATE_' + response.data.key) }).then(
							function successCallback(response) {
								$scope.optionsData.stateOptions = response.data;
							},
							function errorCallback(response) {
							}
						);
				},
				function errorCallback(response) {
				}
			);
		}
	}

	$scope.optionsData = {};
	$http({ method: 'GET', url: ApiResourceConfig.APPLN_TYPES }).then(
		function successCallback(response) {
			$scope.optionsData.applnTypeOptions = response.data;
		},
		function errorCallback(response) {
		}
	);

	$http({ method: 'GET', url: ApiResourceConfig.CODES_BY_CATEGORY('COUNTRY') }).then(
		function successCallback(response) {
			$scope.optionsData.countryOptions = response.data;
		},
		function errorCallback(response) {
		}
	);

	$http({ method: 'GET', url: ApiResourceConfig.CODES_BY_CATEGORY('RACE') }).then(
		function successCallback(response) {
			$scope.optionsData.raceOptions = response.data;
		},
		function errorCallback(response) {
		}
	);

	$http({ method: 'GET', url: ApiResourceConfig.CODES_BY_CATEGORY('GENDER') }).then(
		function successCallback(response) {
			$scope.optionsData.genderOptions = response.data;
		},
		function errorCallback(response) {
		}
	);

	$http({ method: 'GET', url: ApiResourceConfig.CODES_BY_CATEGORY('LANGUAGE') }).then(
		function successCallback(response) {
			$scope.optionsData.languageOptions = response.data;
		},
		function errorCallback(response) {
		}
	);

	$http({ method: 'GET', url: ApiResourceConfig.CODES_BY_CATEGORY('RELIGION') }).then(
		function successCallback(response) {
			$scope.optionsData.religionOptions = response.data;
		},
		function errorCallback(response) {
		}
	);

	if ($scope.data.applicant.birthCountryCd && $scope.data.applicant.birthCountryCd != '') {
		$http({ method: 'GET', url: ApiResourceConfig.CODE_BY_ID($scope.data.applicant.birthCountryCd) }).then(
			function successCallback(response) {
				$http({ method: 'GET', url: ApiResourceConfig.CODES_BY_CATEGORY('STATE_' + response.data.key) }).then(
						function successCallback(response) {
							$scope.optionsData.stateOptions = response.data;
						},
						function errorCallback(response) {
						}
					);
			},
			function errorCallback(response) {
			}
		);
	}
});

testlabApp.controller('APPLN.ApplyNewApplnStep3Ctrl', function($scope, $location, $state, $auth, $stateParams, $http, ApiResourceConfig) {
	if ($scope.$parent.data.pageState.step2Completed == false) {
		$state.go('applnApplyNew.1');
		return;
	}

	$scope.$parent.data.pageState.currentStep = 3;

	$scope.data.application = $scope.$parent.data.application;
	$scope.data.applicant = $scope.$parent.data.applicant;

	$scope.display = {};
	$scope.display.applicant = {};
	$scope.display.application = {};
	if ($scope.data.application.applicationTypeCd && $scope.data.application.applicationTypeCd != '') {
		$http({ method: 'GET', url: ApiResourceConfig.APPLN_TYPE_BY_ID($scope.data.application.applicationTypeCd) }).then(
			function successCallback(response) {
				$scope.display.application.applicationType = response.data;
			},
			function errorCallback(response) {
			}
		);
	}

	if ($scope.data.applicant.genderCd && $scope.data.applicant.genderCd != '') {
		$http({ method: 'GET', url: ApiResourceConfig.CODE_BY_ID($scope.data.applicant.genderCd) }).then(
			function successCallback(response) {
				$scope.display.applicant.gender = response.data;
			},
			function errorCallback(response) {
			}
		);
	}

	if ($scope.data.applicant.birthCountryCd && $scope.data.applicant.birthCountryCd != '') {
		$http({ method: 'GET', url: ApiResourceConfig.CODE_BY_ID($scope.data.applicant.birthCountryCd) }).then(
			function successCallback(response) {
				$scope.display.applicant.birthCountry = response.data;
			},
			function errorCallback(response) {
			}
		);
	}

	if ($scope.data.applicant.raceCd && $scope.data.applicant.raceCd != '') {
		$http({ method: 'GET', url: ApiResourceConfig.CODE_BY_ID($scope.data.applicant.raceCd) }).then(
			function successCallback(response) {
				$scope.display.applicant.race = response.data;
			},
			function errorCallback(response) {
			}
		);
	}

	if ($scope.data.applicant.religionCd && $scope.data.applicant.religionCd != '') {
		$http({ method: 'GET', url: ApiResourceConfig.CODE_BY_ID($scope.data.applicant.religionCd) }).then(
			function successCallback(response) {
				$scope.display.applicant.religion = response.data;
			},
			function errorCallback(response) {
			}
		);
	}

	$scope.display.applicant.languageDisplay = '';
	for (var prop in $scope.data.applicant.language) {
		if ($scope.data.applicant.language[prop] == true) {
			$http({ method: 'GET', url: ApiResourceConfig.CODE_BY_ID(prop) }).then(
				function successCallback(response) {
					$scope.display.applicant.languageDisplay += ($scope.display.applicant.languageDisplay == '' ? '' : ', ') + response.data.value;
				},
				function errorCallback(response) {
				}
			);
		}
	}

	$scope.display.applicant.birthState = null;
	if ($scope.data.applicant.birthStateCd && $scope.data.applicant.birthStateCd != '') {
		$http({ method: 'GET', url: ApiResourceConfig.CODE_BY_ID($scope.data.applicant.birthStateCd) }).then(
			function successCallback(response) {
				$scope.display.applicant.birthState = response.data;
			},
			function errorCallback(response) {
			}
		);
	} else {

	}


	$scope.submit = function() {
		if ($scope.backBtnClicked == true) {
			$state.go('applnApplyNew.2');
			return;
		} else {
			var submitData = {
				application : $scope.$parent.data.application,
				applicant : $scope.$parent.data.applicant
			}

			submitData.applicant.dobTimestamp = null;
			try {
				submitData.applicant.dobTimestamp = submitData.applicant.dob.getTime()
			} catch (error) {}

			$http.post(ApiResourceConfig.BASE_URL + '/appln/submit', submitData)
			.then(
				function success(response) {
					$scope.$parent.data.pageState.step3Completed = true;
					$scope.$parent.data.createdApplication = response.data;
					$state.go('applnApplyNew.4');
				}
			).catch(function(error) {
				$scope.error = true;
				$scope.errorMsg = error.data.message;
	        });
		}
	}
});

testlabApp.controller('APPLN.ApplyNewApplnStep4Ctrl', function($scope, $location, $state, $auth, $stateParams, $http, ApiResourceConfig) {
	if ($scope.$parent.data.pageState.step3Completed == false) {
		$state.go('applnApplyNew.1');
		return;
	}

	$scope.$parent.data.pageState.currentStep = 4;

	$scope.data = $scope.$parent.data;

	$scope.back = function(stepNo) {
		$location.path('#');
	}
});

testlabApp.controller('APPLN.EnquireApplnCtrl', function($scope, $http, $location, ApiResourceConfig, NgTableParams) {
	$scope.optionsData = {};
	$http({ method: 'GET', url: ApiResourceConfig.APPLN_TYPES }).then(
		function successCallback(response) {
			$scope.optionsData.applnTypeOptions = response.data;
		},
		function errorCallback(response) {
		}
	);
	$http({ method: 'GET', url: ApiResourceConfig.CODES_BY_CATEGORY('APPLN_STATUS') }).then(
			function successCallback(response) {
				$scope.optionsData.applicationStatusOptions = response.data;
			},
			function errorCallback(response) {
			}
		);

	$scope.data = $scope.data || {};
	$scope.search = function() {
		$scope.data.result = new NgTableParams({ page: 1, count: 10, sorting : {createdOn : 'desc'} }, {
			getData: function($defer, params) {
				var submitData = {
					criteria : $scope.data.criteria || {},
					page : {
						pageNo : $defer.page(),
						limit : $defer.count()
					},
					sorting : $defer.sorting()
				};

				return $http.post(ApiResourceConfig.APPLN_PUBLIC_SEARCH, submitData).then(
					function successCallback(response) {
						$defer.total(response.data.totalElements);
						return response.data.content;
					},
					function errorCallback(response) {
					}
				);
			}
		});
	}

	$scope.withdrawAppln = function(applnId) {
		$http.post(ApiResourceConfig.APPLN_WITHDRAW(applnId)).then(
			function successCallback(response) {
				aaa = $scope.data.result;
				$scope.data.result.reload();
			},
			function errorCallback(response) {
			}
		);
	}

	$scope.back = function(stepNo) {
		$location.path('#');
	}
});

testlabApp.controller('TASK.TaskMainCtrl', function($scope, $location, $state, $auth, $stateParams, $http, ApiResourceConfig) {
});

testlabApp.controller('TASK.TaskListCtrl', function($scope, $location, $state, $auth, $stateParams, $http, ApiResourceConfig, NgTableParams) {
	$scope.processAppln = function(applicationId) {
		$state.go('task.process', { applicationId : applicationId });
	}

	$scope.data = $scope.data || {};
	$scope.data.result = new NgTableParams({ page: 1, count: 10, sorting : {createdOn : 'desc'} }, {
		getData: function($defer, params) {
			var submitData = {
				criteria : $scope.data.criteria || {},
				page : {
					pageNo : $defer.page(),
					limit : $defer.count()
				},
				sorting : $defer.sorting()
			};

			return $http.post(ApiResourceConfig.APPLN_PO_LIST, submitData).then(
				function successCallback(response) {
					$defer.total(response.data.totalElements);
					return response.data.content;
				},
				function errorCallback(response) {
				}
			);
		}
	});
});

testlabApp.controller('TASK.TaskProcessCtrl', function($scope, $location, $state, $auth, $stateParams, $http, ApiResourceConfig, NgTableParams) {
	var applicationId = $stateParams.applicationId;

	$scope.data = $scope.data || {};
	$scope.display = {};
	$http({ method: 'GET', url: ApiResourceConfig.APPLN_BY_ID(applicationId) }).then(
		function successCallback(response) {
			$scope.data.application = response.data;

			$scope.display.languageDisplay = '';
			if (response.data.applicantLanguages && response.data.applicantLanguages.length) {
				for (var i=0; i<response.data.applicantLanguages.length; i++) {
					$scope.display.languageDisplay += ($scope.display.languageDisplay == '' ? '' : ', ') + response.data.applicantLanguages[i].value
				}
			}
		},
		function errorCallback(response) {
		}
	);

	$scope.back = function() {
		$state.go('task.list');
	}

	$scope.submit = function() {
		console.log($scope.data.decision);
		$http.post($scope.data.decision == 'Approve' ? ApiResourceConfig.APPLN_APPROVE(applicationId) : ApiResourceConfig.APPLN_REJECT(applicationId)).then(
			function successCallback(response) {
				$state.go('task.list');
			},
			function errorCallback(response) {
			}
		);

	}
});
