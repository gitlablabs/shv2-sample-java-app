*** Settings ***
Library     Collections
Library     RequestsLibrary
Suite Teardown  Delete All Sessions

*** Variables ***
${login_json}         {"username":"${VALID USER}", "password": "${VALID PASSWORD}"}




*** Test Cases ***
Login Api
    Get Token

Get Internship Programme Application
    Get Application Types   0   1   NEW_A   Internship Programme
    Get Application Types   1   2   NEW_B   Specialist Manpower Programme
    Get Application Types   2   3   NEW_C   Technology Associate Programme



*** Keywords ***
Get Token
    Create Session                  clglabapi       ${API_SERVER}
    ${resp}=                        Post Request     clglabapi      ${API_ENDPOINT_LOGIN}   data=${login_json}
    Should Be Equal As Strings                      ${resp.status_code}     200
    Dictionary Should Contain Key   ${resp.json()}  token
    ${API TOKEN}=   Get From Dictionary     ${resp.json()}  token
    ${AUTH_HEADER}=    Create Dictionary    Authorization=${API TOKEN}
    set global variable  ${AUTH_HEADER}


Get Application Types
    [Arguments]      ${json_id}      ${application_id}    ${application_code}     ${application_description}
#    log     ${application_id}
#    log     ${application_code}
#    log     ${application_description}
#    log     ${AUTH_HEADER}

    Create Session                  clglabapi       ${API_SERVER}
    ${resp}=	                    Get Request	    clglabapi       ${API_ENDPOINT_GET_APPLICATION_TYPES}   headers=${AUTH_HEADER}
    Should Be Equal As Strings	    ${resp.status_code}	    200
    ${api_result}   set variable    ${resp.json()}
    log  ${api_result}

    ${result1}     Get From List    ${api_result}   ${json_id}
    ${result1_id}  Get From Dictionary     ${result1}  id
    ${result1_code}  Get From Dictionary     ${result1}  code
    ${result1_description}  Get From Dictionary     ${result1}  description
    should be equal as strings     ${result1_id}  ${application_id}
    should be equal as strings      ${result1_code}  ${application_code}
    should be equal as strings      ${result1_description}  ${application_description}