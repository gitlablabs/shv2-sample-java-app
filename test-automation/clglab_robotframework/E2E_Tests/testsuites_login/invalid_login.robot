*** Settings ***
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser
Test Setup        Go To Login Page
Test Template     Login With Invalid Credentials Should Fail
Resource          ../../resources/web_common.robot

*** Test Cases ***               User Name        Password              Error Message
Invalid Username                 invalid          ${VALID PASSWORD}     Authentication Failed: Invalid User Name/ Password
Invalid Password                 ${VALID USER}    invalid               Authentication Failed: Invalid User Name/ Password
Invalid Username And Password    invalid          whatever              Authentication Failed: Invalid User Name/ Password
Empty Username                   ${EMPTY}         ${VALID PASSWORD}     userName is required
#Empty Password                   ${VALID USER}    ${EMPTY}              Authentication Failed: Invalid User Name/ Password
#Empty Username And Password      ${EMPTY}         ${EMPTY}              userName is required

*** Keywords ***
Login With Invalid Credentials Should Fail
    [Arguments]    ${username}    ${password}   ${errorMsg}
    Input Username    ${username}
    Input Password    ${password}
    Submit Credentials
    Login Should Have Failed    ${errorMsg}

Login Should Have Failed
    [Arguments]    ${errorMsg}
    Wait Until Page Contains    ${errorMsg}
