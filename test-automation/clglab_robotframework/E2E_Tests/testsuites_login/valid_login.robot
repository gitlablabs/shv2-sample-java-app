*** Settings ***
Documentation     A test suite with a single Gherkin style test.
...
...               This test is to test successfully login performed by normal user.
Test Teardown     Close Browser
Resource          ../../resources/web_common.robot

*** Test Cases ***
Valid Login
    Given browser is opened to login page
    When user "${VALID USER}" logs in with password "${VALID PASSWORD}"
    Then welcome page should be open

*** Keywords ***
Browser is opened to login page
    Open browser to login page

User "${username}" logs in with password "${password}"
    Input username    ${username}
    Input password    ${password}
    Submit credentials


Welcome Page Should Be Open
    Wait Until Page Contains    Greeting Public User
    Title Should Be    Test Lab Sample Web Application