*** Settings ***
Library    AppiumLibrary
Library    OperatingSystem
Library    Process
Library    ../resources/lib/CustomLibrary.py

*** Keywords ***
Open the application
    [Arguments]    ${command}
    Print  Open app here
    Start the appium server    ${command}
    Open Application    http://127.0.0.1:4723/wd/hub


Open the Chrome broswer
    [Arguments]    ${command}
    Print  Open app here
    Start the appium server for browser    ${command}
    Open Application    http://127.0.0.1:4723/wd/hub

Close the application
    Print  close app here
    Close Application
    Kill Node Instances

Start the appium server
    [Arguments]    ${appium_command}
    Print  start the appium server here
    Kill Node Instances
    Print    ${appium_command}
    CustomLibrary.Start Appium Server  ${appium_command}
    Sleep   10s

Start the appium server for browser
    [Arguments]    ${appium_command_browser}
    Print  start the appium server here
    Kill Node Instances
    Print    ${appium_command_browser}
    CustomLibrary.Start Appium Server  ${appium_command_browser}
    Sleep   10s

Kill Node Instances
    OperatingSystem.Run    taskkill.exe /F /IM node.exe

Click
    [Arguments]    ${id}
    Wait Until Page Contains Element    ${id}
    Click Element    ${id}

Type
    [Arguments]    ${id}   ${text}
    Wait Until Page Contains Element    ${id}
    Input Text   ${id}     ${text}
    Hide Keyboard

Print
    [Arguments]  ${msg}
    Log  ${msg}