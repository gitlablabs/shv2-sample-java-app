*** Settings ***
Library           SeleniumLibrary

*** Variables ***
${DELAY}          0
${LOGIN URL}      http://${E2E_SERVER}/#!/login


*** Keywords ***
Open Browser To Login Page
    log  ${LOGIN URL}
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Login Page Should Be Open

Login Page Should Be Open
    Title Should Be    Test Lab Sample Web Application

Go To Login Page
    Go To    ${LOGIN URL}
    Login Page Should Be Open

Input Username
    [Arguments]    ${username}
    Input Text    username    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    password    ${password}

Submit Credentials
    Click Button   Login



