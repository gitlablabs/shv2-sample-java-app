###### robotframework


# Robotframework test script


[Robot Framework](https://robotframework.org) is a generic open source test automation framework and
[SeleniumLibrary](https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html) is one of the many test libraries that can be used with
it. 

In addition to showing how they can be used together for web and api testing.
This demo introduces the basic Robot Framework test data syntax, how tests
are executed, and how logs and reports look like.


-------------------

### API Test Suites
1. testsuites_api
    - Stored API tests
    
###Test Cases
1. api_test.robot
    - Endpoints 
        1. Get Token
        2. Retrieve application types

#### Using Argument File To Run Tests

> Running the tests with the parameters from the text file

    ```python3 -m robot.run -A settingsfile.txt API_Tests/testsuites_api/api_test.robot```
    
> Passing in parameters (Local Terminal) 
    ```python3 -m robot.run -T -d testreports/  -A settingsfile.txt --variable API_SERVER:http://13.229.67.52 API_Tests/testsuites_api/api_test.robot``` 

>> Passing in parameters (Server) 
    ```robot -T -d testreports/  -A settingsfile.txt --variable API_SERVER:http://13.229.67.52 API_Tests/testsuites_api/api_test.robot``` 

###### Note:
[Rest Instance Library](https://asyrjasalo.github.io/RESTinstance/)
[API Dictionary](https://robotframework.org/robotframework/latest/libraries/Collections.html)

-------------------

### E2E Test Suites
1. testsuites_login
   - Login feature test cases are located in the testsuites_login directory.
   

###Test Cases
1. valid_login.robot
    - A test suite with a single test for valid login.
    - A test suite with a single Gherkin style test.
2. invalid_login.robot
    - A test suite containing tests related to invalid login.
    - These tests are data-driven by their nature. They use a single keyword, specified with the Test Template setting, that is called with different arguments to cover different scenarios.

#### Using Argument File
> Running the tests with the parameters from the text file

    ```python3 -m robot.run -A settingsfile.txt E2E_Tests/testsuites_login/valid_login.robot```

> The browser that is used is controlled by ${BROWSER} variable defined in settingsfile (textfile) Chrome browser is used by default, but that can be easily overridden from the command line:

    ```python3 -m robot.run -A settingsfile.txt --variable BROWSER:firefox E2E_Tests/testsuites_login/valid_login.robot```

>> Passing in parameters (Server) 
    ```robot -T -d testreports/  -A settingsfile.txt --variable BROWSER:headlesschrome --variable E2E_SERVER:13.229.67.52 E2E_Tests/testsuites_login``` 

-------------------

### Run Test Suite
The test cases are located in the testsuites_login directory. They can be executed using the robot command:

```python3 -m robot.run -A settingsfile.txt E2E_Tests/testsuites_login```

       
###### *Note*
###### If you are using Robot Framework 2.9 or earlier, you need to use the pybot command instead.

You can also run an individual test case file and use various command line options supported by Robot Framework:

```python3 -m robot.run -A settingsfile.txt E2E_Tests/testsuites_login/valid_login.robot```

-------------------




###### *Update chromedriver in Mac:*
```
brew info chromedriver
brew tap homebrew/cask
brew cask install chromedriver
```
-------------------

