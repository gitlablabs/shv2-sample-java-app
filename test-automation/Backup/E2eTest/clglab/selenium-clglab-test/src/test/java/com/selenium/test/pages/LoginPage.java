package com.selenium.test.pages;


import com.selenium.framework.base.SeleniumBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope("cucumber-glue")
public class LoginPage extends SeleniumBase {


    @FindBy(xpath = "/html/body/nav/div/div[2]/ul[2]/li/a")
    public static WebElement LOGIN_LINK;


    @FindBy(xpath = "/html/body/div[1]/form/div/div[3]/button")
    public static WebElement LOGIN_BUTTON;

    @FindBy(xpath = "/html/body/nav/div/div[2]/ul[2]/li/a")
    public static WebElement LOGOUT_BUTTON;

    @FindBy(id = "username")
    public static WebElement USERNAME;

    @FindBy(id = "password")
    public static WebElement PASSWORD;

    @FindBy(xpath = "/html/body/nav/div/div[2]/ul[1]/li[1]/a")
    public static WebElement APPLY_NEW_APPLICATION;

    @FindBy(xpath = "/html/body/nav/div/div[2]/ul[1]/li[2]/a")
    public static WebElement Enquire_Application_Status;

    @FindBy(xpath = "/html/body/div[1]/h1")
    public static WebElement BEFORE_LOGIN_GREETING;

    @FindBy(xpath = "/html/body/div[1]/div")
    public static WebElement LOGIN_ERROR_MESSAGE;






    public LoginPage(WebDriver driver) {
        super(driver);
    }


}
