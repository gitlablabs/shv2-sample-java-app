package com.selenium.test.config;

import org.junit.Assert;

import static com.support.framework.support.Util.stringIsEmpty;

public enum TestConfig {

    WEBPAGE_TITLE("Test Lab Sample Web Application"),
    LOGIN_LINK_TEXT("Login"),
    APPLY_NEW_APPLICATION_TEXT("Apply New Application"),
    Enquire_Application_Status_TEXT("Enquire Application Status"),
    BEFORE_LOGIN_GREETING_TEXT("Greeting");



    private String value;

    TestConfig(String value) {
        this.value = value;
    }

    public boolean toBoolean() {
        if (stringIsEmpty(value)) {
            Assert.fail("TestConfig " + this.name() + " is missing. Please check TestConfig");
        }
        return Boolean.parseBoolean(value);
    }

    public int toInt() {
        if (stringIsEmpty(value)) {
            Assert.fail("TestConfig " + this.name() + " is missing. Please check TestConfig");
        }
        return Integer.parseInt(value);
    }

    public String toString() {
        if (stringIsEmpty(value)) {
            Assert.fail("TestConfig " + this.name() + " is missing. Please check TestConfig");
        }
        return value;
    }
}



