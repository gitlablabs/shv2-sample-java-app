package com.selenium.test.stepdefs;

import cucumber.api.java.en.And;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.selenium.test.pages.LoginPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import static com.selenium.test.pages.LoginPage.*;
import static com.selenium.test.config.TestConfig.*;


public class LoginPageStepDefs {

    private static final Logger log = Logger.getLogger(LoginPageStepDefs.class);

    @Autowired
    public LoginPage loginPage;

    @Given("^I am on 'Login Page' screen$")
    public void givenIAmOnLoginPageScreen() {
        loginPage.getURL("/#!/home");
        log.info("====> Assert [getWebPageTitle]:" + loginPage.getWebPageTitle());
        Assert.assertEquals(WEBPAGE_TITLE.toString(), loginPage.getWebPageTitle());

    }

    @When("^I click on the Login Link$")
    public void i_click_on_the_Login_Link() throws Exception {
        loginPage.assertElementPresent(LOGIN_LINK, 10);
        Assert.assertEquals("Login text should be Login",LOGIN_LINK_TEXT.toString(),LOGIN_LINK.getText());
        LOGIN_LINK.click();
        loginPage.waitForPageLoaded(2);
    }

    @When("^I fill in username with \"([^\"]*)\"$")
    public void i_fill_in_username_with(String username) throws Exception {
        log.info("====> Username:" + username);
        loginPage.assertElementPresent(USERNAME, 10);
        USERNAME.sendKeys(username);
    }

    @When("^I fill in password with \"([^\"]*)\"$")
    public void i_fill_in_password_with(String pwd) throws Exception {
        log.info("====> Password:" + pwd);
        loginPage.assertElementPresent(PASSWORD, 10);
        PASSWORD.sendKeys(pwd);
    }

    @When("^I click on the \"([^\"]*)\" button$")
    public void i_click_on_the_button(String arg1) throws Exception {
        loginPage.assertElementPresent(LOGIN_BUTTON, 10);
        LOGIN_BUTTON.click();
        loginPage.waitForPageLoaded(2);
    }


    @And("^I should get logged-in$")
    public void i_should_get_logged_in() throws Exception {
        //Assert Successful Login (Assert the two menu options)
        loginPage.assertElementPresent(APPLY_NEW_APPLICATION,10);
        Assert.assertEquals("Assert Apply New Application Text",APPLY_NEW_APPLICATION_TEXT.toString(), APPLY_NEW_APPLICATION.getText());
        loginPage.assertElementPresent(Enquire_Application_Status,10);
        Assert.assertEquals("Assert Enquire Application Status Text",Enquire_Application_Status_TEXT.toString(), Enquire_Application_Status.getText());
    }

    @Then("^I click on the logout button$")
    public void i_click_on_the_logout_button() throws Exception {
        loginPage.assertElementPresent(LOGOUT_BUTTON, 10);
        LOGOUT_BUTTON.click();
        loginPage.waitForPageLoaded(2);
        loginPage.assertElementPresent(BEFORE_LOGIN_GREETING,10);
        Assert.assertEquals("Assert Greeting Text",BEFORE_LOGIN_GREETING_TEXT.toString(), BEFORE_LOGIN_GREETING.getText());

    }


    @Then("^I should see \"([^\"]*)\" message$")
    public void i_should_see_message(String errorMsg) throws Exception {
        loginPage.waitForPageLoaded(5);
        loginPage.assertElementPresent(LOGIN_ERROR_MESSAGE,10);
        Assert.assertEquals("Assert Login Error Message Text",errorMsg, LOGIN_ERROR_MESSAGE.getText());

    }



}
