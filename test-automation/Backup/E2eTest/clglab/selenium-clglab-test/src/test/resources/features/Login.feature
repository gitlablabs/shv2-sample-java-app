# new feature
# Tags: optional

@login
Feature: Sample App Login
  As a user
  I want to login my profile using my credentials
  In order to view the application portal

  Background: User navigates to Sample Application Portal
    Given I am on 'Login Page' screen
    Then I click on the Login Link

  Scenario: Successful login
    When I fill in username with "public"
    And I fill in password with "password"
    And I click on the "Login" button
    And I should get logged-in
    Then I click on the logout button


  Scenario Outline: Failed login using wrong credentials
      When I fill in username with "<username>"
      And I fill in password with "<password>"
      And I click on the "Login" button
      Then I should see "<warning>" message
      Examples:
        | username    | password   | warning                           |
        | Test        | !23        | Authentication Failed: Invalid User Name/ Password |
        | public      | 123        | Authentication Failed: Invalid User Name/ Password |
        | Test        | password   | Authentication Failed: Invalid User Name/ Password |
        | Test        |            | Authentication Failed: Invalid User Name/ Password |
        |             | 123        | userName is required            |
#       |             |            | No content to map due to end-of-input at [Source: org.apache.catalina.connector.CoyoteInputStream@ce2d97b; line: 1, column: 0]    |


