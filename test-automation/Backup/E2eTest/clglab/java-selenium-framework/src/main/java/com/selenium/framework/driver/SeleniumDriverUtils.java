package com.selenium.framework.driver;

import io.github.bonigarcia.wdm.*;
import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.openqa.selenium.Platform;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;


import java.net.MalformedURLException;
import java.net.URL;

import static com.support.framework.support.Property.*;
import static org.junit.Assert.fail;

@Component
class SeleniumDriverUtils {

    private static final Logger LOG = Logger.getLogger(SeleniumDriverUtils.class);

    @Bean(destroyMethod = "quit")
    @Scope("cucumber-glue")
    @Profile("Selenium")
    private WebDriver getWebDriver() {

        LOG.info("Checking Operating System: "+ getOS());

        WebDriver driver;
        TestCapabilities testCapabilities = new TestCapabilities();
        LOG.info("Initializing WebDriver...");
        if (GRID_USE.toString().equalsIgnoreCase("true")) {
            driver = new RemoteWebDriver(testCapabilities.getRemoteUrl(), testCapabilities.getDesiredCapabilities());
        } else {
            switch (BROWSER_NAME.toString().toLowerCase()) {
                case "chrome":
                    if(getOS().equalsIgnoreCase("Linux")){
                        LOG.info("Linux Operating System");

                        System.setProperty("webdriver.chrome.driver",CHROME_SERVER_DRIVER_PATH.toString());

                        ChromeOptions options = new ChromeOptions();
                        options.addArguments("--headless");
                        options.addArguments("--no-sandbox"); // Bypass OS security model
                        options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
                        options.addArguments("nogui=True");
                        options.addArguments("disable-infobars"); // disabling infobars
                        options.addArguments("--disable-extensions"); // disabling extensions

                        driver = new ChromeDriver(options);
                        break;


                    }else{
                        LOG.info("Local");
                        ChromeDriverManager.getInstance().setup();
                        driver = new ChromeDriver(new ChromeOptions());
                        break;
                    }

                case "firefox":
                    if(getOS().equalsIgnoreCase("Linux")){
                        LOG.info("Linux Operating System::Firefox");
                        System.setProperty("webdriver.gecko.driver",FIREFOX_SERVER_DRIVER_PATH.toString());

                        FirefoxOptions firefoxOptions = new FirefoxOptions();
                        firefoxOptions.setCapability(CapabilityType.BROWSER_NAME, BrowserType.FIREFOX);
                        firefoxOptions.setCapability(CapabilityType.PLATFORM_NAME, Platform.LINUX);
                        //firefoxOptions.setHeadless(true);

                        FirefoxBinary firefoxBinary = new FirefoxBinary();
                        firefoxBinary.addCommandLineOptions("--headless");
                        firefoxOptions.setBinary(firefoxBinary);
                        firefoxOptions.setBinary(firefoxBinary);

                        driver = new FirefoxDriver(firefoxOptions);
                    }else{
                        FirefoxDriverManager.getInstance().setup();
                        driver = new FirefoxDriver(new FirefoxOptions());
                    }
                    break;

                case "opera":
                    OperaDriverManager.getInstance().setup();
                    driver = new OperaDriver(new OperaOptions());
                    break;
                case "microsoftedge":
                    EdgeDriverManager.getInstance().setup();
                    driver = new EdgeDriver(new EdgeOptions());
                    break;
                case "ie":
                    InternetExplorerDriverManager.getInstance().setup();
                    driver = new InternetExplorerDriver(new InternetExplorerOptions());
                    break;
                case "safari":
                    driver = new SafariDriver(new SafariOptions());
                    break;
                case "phantomjs":
                    LOG.info("Linux Operating System::PhantomJs");

                    DesiredCapabilities caps = new DesiredCapabilities();
                    caps.setCapability(CapabilityType.BROWSER_NAME, BrowserType.PHANTOMJS);
                    caps.setCapability(CapabilityType.PLATFORM_NAME, Platform.LINUX);
                    caps.setJavascriptEnabled(true);
                    caps.setCapability("locationContextEnabled", true);
                    caps.setCapability("applicationCacheEnabled", true);
                    caps.setCapability("browserConnectionEnabled", true);
                    caps.setCapability("localToRemoteUrlAccessEnabled", true);
                    caps.setCapability("locationContextEnabled", true);
                    caps.setCapability("takesScreenshot", true);


                    if(getOS().equalsIgnoreCase("Linux")){
                        System.setProperty("phantomjs.binary.path", PHANTOMJS_SERVER_DRIVER_PATH.toString());
                        //SERVER
                        caps.setCapability(
                                PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                                PHANTOMJS_SERVER_DRIVER_PATH.toString()
                        );
                    }else{
                        //LOCAL
	                    caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                                "/Users/debbieang/Documents/GovTech/Reference Pipeline/Selenium/phantomjs-2.1.1-macosx/bin/phantomjs");
                    }
                    driver = new PhantomJSDriver(caps);
                    break;
                default:
                    fail(BROWSER_NAME + " is not found in browser list");
                    driver = null;
                    break;
            }
        }
        driver.manage().deleteAllCookies();
        LOG.info("Resizing browser to: " + BROWSER_WIDTH + "x" + BROWSER_HEIGHT);
        driver.manage().window().setSize(new Dimension(BROWSER_WIDTH.toInt(), BROWSER_HEIGHT.toInt()));
        return driver;
    }

    private String getOS(){
        return System.getProperty("os.name");
    }
}