## Running Your Tests

 * `mvn clean install` inside the root path will run all the test modules with default settings.
    After `cd` to submodule, you can only run that module with `mvn clean install`
    You can also `mvn clean install -f selenium-clgtest-test/pom.xml` to run specific submodule

 * If you like to override default run settings you can just run it with

        #This will run your test on Chrome Browser after cd to selenium-clglab-test module
        mvn clean install -Dspring.profiles.active="Selenium" -Dbrowser.name="Chrome" -Dbase.url="http://10.0.1.6"

 * Thus

        #This will run your account test on Chrome Browser
        mvn clean install -f selenium-clglab-test/pom.xml -Dspring.profiles.active="Selenium" -Dbrowser.name="Chrome" -Dbase.url="http://10.0.1.6"


 


### Credits to com.github.mosclofri

### Technology:
 * Appium
 * Cucumber
 * Maven
 * Spring

### Highlights:
 * Selenium and Appium Support
 * Page object pattern
 * Mavenized
 * Behaviour driven development
 * Standardized code across all of your test projects
 * Basic helper methods
 * Business readable HTML test results
 