package clgLab.sampleApplicationTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.PhantomJsDriverUtil;
import utils.SampleWebApplicationPropFile;



public class SampleWebApplicationTest {
	

	  public WebDriver driver ; 
	  
	  
	  @BeforeClass
	  public void setUp() {
		  driver = PhantomJsDriverUtil.setUp(); 
	  }
	  
	  @Test(priority=1)
	  public void launchSampleWebApplication() {
          System.out.println("====> DEBUGGING [baseURL]:" + SampleWebApplicationPropFile.BASE_URL);
		  driver.get(SampleWebApplicationPropFile.BASE_URL);  
		  driver.manage().window().setSize(new Dimension(1920,1080));
		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  }
	     
	  @Test(priority=2)
	  public void verifyHomepageTitleTest() {
	      String expectedTitle = "Test Lab Sample Web Application";
	      String actualTitle = driver.getTitle();
	      Assert.assertEquals(actualTitle, expectedTitle);
	      System.out.println("====> Assert [actualTitle]:" + actualTitle);
	      System.out.println("====> DEBUGGING [current url]:" + driver.getCurrentUrl());     
	  }
	  
	  @Test(priority=3)
	  public void successLoginTest() {
		  By loginLinkLocator = By.xpath("/html/body/nav/div/div[2]/ul[2]/li/a");
		  waitForElement(driver, loginLinkLocator);
		  
		  String actualLoginText = driver.findElement(loginLinkLocator).getText();
		  Assert.assertEquals(actualLoginText, "Login");
		  System.out.println("====> Assert [actualLoginText]:" + actualLoginText);
		  
		  //Click Login link
		  driver.findElement(loginLinkLocator).click();

		  System.out.println("====> DEBUGGING [current url]:" + driver.getCurrentUrl());
		    
		  //Enter username and pwd
		  waitForElement(driver, By.id("username"));
		  driver.findElement(By.id("username")).sendKeys("public");
		  driver.findElement(By.id("password")).sendKeys("password");
		  
		  //Click Login Button
		  driver.findElement(By.xpath("/html/body/div[1]/form/div/div[3]/button")).click();

		  //Assert Successful Login (Assert the two menu options)
		  By applyNewAppLocator = By.xpath("/html/body/nav/div/div[2]/ul[1]/li[1]/a");
		  waitForElement(driver, applyNewAppLocator);
		  String applyNewAppText= driver.findElement(applyNewAppLocator).getText();
		  System.out.println("====> Assert [applyNewAppText]:" + applyNewAppText);
		  Assert.assertEquals(applyNewAppText, "Apply New Application");
		  
		  By enquireAppStatusLocator = By.xpath("/html/body/nav/div/div[2]/ul[1]/li[2]/a");
		  waitForElement(driver, enquireAppStatusLocator);
		  String enquireAppStatusText= driver.findElement(enquireAppStatusLocator).getText();
		  System.out.println("====> Assert [enquireAppStatusText]:" + enquireAppStatusText);
		  Assert.assertEquals(applyNewAppText, "Apply New Application");
		  
		  //Click Logout 
		  driver.findElement(By.xpath("/html/body/nav/div/div[2]/ul[2]/li/a")).click();
		  
	  }
	  
	  @Test(priority=4)
	  public void failLoginTest() {
		  By loginLinkLocator = By.xpath("/html/body/nav/div/div[2]/ul[2]/li/a");
		  waitForElement(driver, loginLinkLocator);
		  
		  String actualLoginText = driver.findElement(loginLinkLocator).getText();
		  Assert.assertEquals(actualLoginText, "Login");
		  System.out.println("====> Assert [actualLoginText]:" + actualLoginText);
		  
		  //Click Login link
		  driver.findElement(loginLinkLocator).click();

		  System.out.println("====> DEBUGGING [current url]:" + driver.getCurrentUrl());
		  
  
		  //Enter username and pwd
		  waitForElement(driver, By.id("username"));
		  driver.findElement(By.id("username")).sendKeys("public2");
		  driver.findElement(By.id("password")).sendKeys("password");
		  
		  //Click Login Button
		  driver.findElement(By.xpath("/html/body/div[1]/form/div/div[3]/button")).click();

		  //Assert Successful Login (Assert the two menu options)
		  By applyNewAppLocator = By.xpath("/html/body/nav/div/div[2]/ul[1]/li[1]/a");
		  waitForElement(driver, applyNewAppLocator);
		  String applyNewAppText= driver.findElement(applyNewAppLocator).getText();
		  System.out.println("====> Assert [applyNewAppText]:" + applyNewAppText);
		  Assert.assertEquals(applyNewAppText, "Apply New Application");  
		  
	  }
	  
	  private void  waitForElement(WebDriver driver, By locator) {
		  WebDriverWait wait = new WebDriverWait(driver, 10);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	  }
	  
	  @AfterClass
	  public void tearDown() {
		  driver.close();
	  }

}
