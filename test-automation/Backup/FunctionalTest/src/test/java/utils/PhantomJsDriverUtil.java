package utils;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import utils.SampleWebApplicationPropFile;

public class PhantomJsDriverUtil {

	 public static WebDriver setUp() {
		 
		 System.setProperty("phantomjs.binary.path", SampleWebApplicationPropFile.PHANTOMJS_SERVER_DRIVER_PATH);
	     DesiredCapabilities caps = new DesiredCapabilities(); 	
	     caps.setCapability(CapabilityType.BROWSER_NAME, BrowserType.PHANTOMJS);
	     caps.setCapability(CapabilityType.PLATFORM_NAME, Platform.LINUX);
	     caps.setJavascriptEnabled(true);
	     caps.setCapability("locationContextEnabled", true);
	     caps.setCapability("applicationCacheEnabled", true);
	     caps.setCapability("browserConnectionEnabled", true);
	     caps.setCapability("localToRemoteUrlAccessEnabled", true);
	     caps.setCapability("locationContextEnabled", true);               
	     caps.setCapability("takesScreenshot", true);  
	      
	     //SERVER
	     caps.setCapability(
	                             PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
	                             SampleWebApplicationPropFile.PHANTOMJS_SERVER_DRIVER_PATH
	                        );
	      
//	     //LOCAL
//	     caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
//	                             "C:\\Automated Tools\\#2(b)_Selenium_WebDriver\\phantomjs-2.1.1-windows\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe"
//	                         );

	      return new  PhantomJSDriver(caps); 
	  }
	
}
