package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFileUtil {
	
	public static String getWebServerIpAddress() {
		Properties prop = new Properties();
		InputStream input = null;
		String webServerIpAdd = "";
		
		try {

			//Local Testing
			//input = new FileInputStream("C:\\Users\\User\\Documents\\CLG Pipeline\\Selenium\\ipaddresses.properties");
			
			//Server
			input = new FileInputStream(SampleWebApplicationPropFile.IP_ADDRESS_PROPERTIES_FILE);

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			webServerIpAdd = prop.getProperty("webserver_ip_address");
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return webServerIpAdd;
	}

}
